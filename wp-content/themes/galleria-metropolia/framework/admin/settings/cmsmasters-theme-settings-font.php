<?php 
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version		1.0.6
 * 
 * Admin Panel Fonts Options
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_options_font_tabs() {
	$tabs = array();
	
	$tabs['content'] = esc_attr__('Content', 'galleria-metropolia');
	$tabs['link'] = esc_attr__('Links', 'galleria-metropolia');
	$tabs['nav'] = esc_attr__('Navigation', 'galleria-metropolia');
	$tabs['heading'] = esc_attr__('Heading', 'galleria-metropolia');
	$tabs['other'] = esc_attr__('Other', 'galleria-metropolia');
	$tabs['google'] = esc_attr__('Google Fonts', 'galleria-metropolia');
	
	return apply_filters('cmsmasters_options_font_tabs_filter', $tabs);
}


function galleria_metropolia_options_font_sections() {
	$tab = galleria_metropolia_get_the_tab();
	
	switch ($tab) {
	case 'content':
		$sections = array();
		
		$sections['content_section'] = esc_html__('Content Font Options', 'galleria-metropolia');
		
		break;
	case 'link':
		$sections = array();
		
		$sections['link_section'] = esc_html__('Links Font Options', 'galleria-metropolia');
		
		break;
	case 'nav':
		$sections = array();
		
		$sections['nav_section'] = esc_html__('Navigation Font Options', 'galleria-metropolia');
		
		break;
	case 'heading':
		$sections = array();
		
		$sections['heading_section'] = esc_html__('Headings Font Options', 'galleria-metropolia');
		
		break;
	case 'other':
		$sections = array();
		
		$sections['other_section'] = esc_html__('Other Fonts Options', 'galleria-metropolia');
		
		break;
	case 'google':
		$sections = array();
		
		$sections['google_section'] = esc_html__('Serving Google Fonts from CDN', 'galleria-metropolia');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return apply_filters('cmsmasters_options_font_sections_filter', $sections, $tab);
} 


function galleria_metropolia_options_font_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = galleria_metropolia_get_the_tab();
	}
	
	
	$options = array();
	
	
	$defaults = galleria_metropolia_settings_font_defaults();
	
	
	switch ($tab) {
	case 'content':
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_content_font', 
			'title' => esc_html__('Main Content Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_content_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style' 
			) 
		);
		
		break;
	case 'link':
		$options[] = array( 
			'section' => 'link_section', 
			'id' => 'galleria-metropolia' . '_link_font', 
			'title' => esc_html__('Links Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_link_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'link_section', 
			'id' => 'galleria-metropolia' . '_link_hover_decoration', 
			'title' => esc_html__('Links Hover Text Decoration', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_link_hover_decoration'], 
			'choices' => galleria_metropolia_text_decoration_list() 
		);
		
		break;
	case 'nav':
		$options[] = array( 
			'section' => 'nav_section', 
			'id' => 'galleria-metropolia' . '_nav_title_font', 
			'title' => esc_html__('Navigation Title Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_nav_title_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform' 
			) 
		);
		
		$options[] = array( 
			'section' => 'nav_section', 
			'id' => 'galleria-metropolia' . '_nav_dropdown_font', 
			'title' => esc_html__('Navigation Dropdown Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_nav_dropdown_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform' 
			) 
		);
		
		break;
	case 'heading':
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'galleria-metropolia' . '_h1_font', 
			'title' => esc_html__('H1 Tag Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_h1_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'galleria-metropolia' . '_h2_font', 
			'title' => esc_html__('H2 Tag Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_h2_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'galleria-metropolia' . '_h3_font', 
			'title' => esc_html__('H3 Tag Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_h3_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'galleria-metropolia' . '_h4_font', 
			'title' => esc_html__('H4 Tag Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_h4_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'galleria-metropolia' . '_h5_font', 
			'title' => esc_html__('H5 Tag Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_h5_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		$options[] = array( 
			'section' => 'heading_section', 
			'id' => 'galleria-metropolia' . '_h6_font', 
			'title' => esc_html__('H6 Tag Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_h6_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform', 
				'text_decoration' 
			) 
		);
		
		break;
	case 'other':
		$options[] = array( 
			'section' => 'other_section', 
			'id' => 'galleria-metropolia' . '_button_font', 
			'title' => esc_html__('Button Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_button_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform' 
			) 
		);
		
		$options[] = array( 
			'section' => 'other_section', 
			'id' => 'galleria-metropolia' . '_small_font', 
			'title' => esc_html__('Small Tag Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_small_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style', 
				'text_transform' 
			) 
		);
		
		$options[] = array( 
			'section' => 'other_section', 
			'id' => 'galleria-metropolia' . '_input_font', 
			'title' => esc_html__('Text Fields Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_input_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style' 
			) 
		);
		
		$options[] = array( 
			'section' => 'other_section', 
			'id' => 'galleria-metropolia' . '_quote_font', 
			'title' => esc_html__('Blockquote Font', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'typorgaphy', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_quote_font'], 
			'choices' => array( 
				'system_font', 
				'google_font', 
				'font_size', 
				'line_height', 
				'font_weight', 
				'font_style' 
			) 
		);
		
		break;
	case 'google':
		$options[] = array( 
			'section' => 'google_section', 
			'id' => 'galleria-metropolia' . '_google_web_fonts', 
			'title' => esc_html__('Google Fonts', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'google_web_fonts', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_google_web_fonts'] 
		);
		
		$options[] = array( 
			'section' => 'google_section', 
			'id' => 'galleria-metropolia' . '_google_web_fonts_subset', 
			'title' => esc_html__('Google Fonts Subset', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select_multiple', 
			'std' => '', 
			'choices' => array( 
				esc_html__('Latin Extended', 'galleria-metropolia') . '|' . 'latin-ext', 
				esc_html__('Arabic', 'galleria-metropolia') . '|' . 'arabic', 
				esc_html__('Cyrillic', 'galleria-metropolia') . '|' . 'cyrillic', 
				esc_html__('Cyrillic Extended', 'galleria-metropolia') . '|' . 'cyrillic-ext', 
				esc_html__('Greek', 'galleria-metropolia') . '|' . 'greek', 
				esc_html__('Greek Extended', 'galleria-metropolia') . '|' . 'greek-ext', 
				esc_html__('Vietnamese', 'galleria-metropolia') . '|' . 'vietnamese', 
				esc_html__('Japanese', 'galleria-metropolia') . '|' . 'japanese', 
				esc_html__('Korean', 'galleria-metropolia') . '|' . 'korean', 
				esc_html__('Thai', 'galleria-metropolia') . '|' . 'thai', 
				esc_html__('Bengali', 'galleria-metropolia') . '|' . 'bengali', 
				esc_html__('Devanagari', 'galleria-metropolia') . '|' . 'devanagari', 
				esc_html__('Gujarati', 'galleria-metropolia') . '|' . 'gujarati', 
				esc_html__('Gurmukhi', 'galleria-metropolia') . '|' . 'gurmukhi', 
				esc_html__('Hebrew', 'galleria-metropolia') . '|' . 'hebrew', 
				esc_html__('Kannada', 'galleria-metropolia') . '|' . 'kannada', 
				esc_html__('Khmer', 'galleria-metropolia') . '|' . 'khmer', 
				esc_html__('Malayalam', 'galleria-metropolia') . '|' . 'malayalam', 
				esc_html__('Myanmar', 'galleria-metropolia') . '|' . 'myanmar', 
				esc_html__('Oriya', 'galleria-metropolia') . '|' . 'oriya', 
				esc_html__('Sinhala', 'galleria-metropolia') . '|' . 'sinhala', 
				esc_html__('Tamil', 'galleria-metropolia') . '|' . 'tamil', 
				esc_html__('Telugu', 'galleria-metropolia') . '|' . 'telugu' 
			) 
		);
		
		break;
	}
	
	return apply_filters('cmsmasters_options_font_fields_filter', $options, $tab);	
}

