<?php
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version 	1.0.4
 * 
 * Theme Primary Color Schemes Rules
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_theme_colors_primary() {
	$cmsmasters_option = galleria_metropolia_get_global_options();
	
	
	$cmsmasters_color_schemes = cmsmasters_color_schemes_list();
	
	
	$custom_css = "/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version 	1.0.4
 * 
 * Theme Primary Color Schemes Rules
 * Created by CMSMasters
 * 
 */

";
	
	
	foreach ($cmsmasters_color_schemes as $scheme => $title) {
		$rule = (($scheme != 'default') ? "html .cmsmasters_color_scheme_{$scheme} " : '');
		
		
		$custom_css .= "
/***************** Start {$title} Color Scheme Rules ******************/

	/* Start Main Content Font Color */
	" . (($scheme == 'default') ? "body," : '') . "
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme}," : '') . "
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	{$rule}textarea,
	{$rule}select,
	{$rule}option, 
	{$rule}.cmsmasters_open_post .cmsmasters_post_tags, 
	{$rule}.cmsmasters_profile .cmsmasters_profile_subtitle, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item .feature_list, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item .feature_list a, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_subtitle, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_subtitle, 
	{$rule}.widget_custom_contact_info_entries, 
	{$rule}.widget_custom_contact_info_entries a, 
	{$rule}.widget_pages ul li a, 
	{$rule}.widget_nav_menu ul li a {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_color']) . "
	}
	
	{$rule}input::-webkit-input-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_color']) . "
	}
	
	{$rule}input:-moz-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_color']) . "
	}
	/* Finish Main Content Font Color */
	
	
	/* Start Primary Color */
	{$rule}a,
	{$rule}h1 a:hover,
	{$rule}h2 a:hover,
	{$rule}h3 a:hover,
	{$rule}h4 a:hover,
	{$rule}h5 a:hover,
	{$rule}h6 a:hover,
	{$rule}.color_2,
	{$rule}.cmsmasters_dropcap.type1,
	{$rule}.cmsmasters_icon_wrap a .cmsmasters_simple_icon,
	{$rule}.cmsmasters_wrap_more_items.cmsmasters_loading:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_top:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_heading_left .icon_box_heading:before,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_icon:before,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_horizontal .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner:before, 
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner:before, 
	{$rule}.bypostauthor > .comment-body .alignleft:before,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > a:hover,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > a:hover,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > a:hover,
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_edit a, 
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_meta a, 
	{$rule}.cmsmasters_open_post .cmsmasters_post_tags, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_cont_info > a, 
	{$rule}.cmsmasters_archive_type .cmsmasters_archive_item_info > a, 
	{$rule}.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list li a:hover, 
	{$rule}.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list li.current a:hover, 
	{$rule}.cmsmasters_twitter_wrap .published, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_icon, 
	{$rule}.cmsmasters_quotes_slider.cmsmasters_quotes_slider_type_box .cmsmasters_quote_content:before,  
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_content:before, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_title, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_content:before, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_title, 
	{$rule}.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tab.tab_comments li small, 
	{$rule}.widget_custom_twitter_entries .tweet_time, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_date_wrap a:hover, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_date_wrap a:hover .cmsmasters_post_date {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	" . (($scheme == 'default') ? "mark," : '') . "
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme} mark," : '') . "
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_top:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left_top:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left:before,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_bg .cmsmasters_icon_list_item .cmsmasters_icon_list_icon,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_item:hover .cmsmasters_icon_list_icon,
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=checkbox] + span.wpcf7-list-item-label:after, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=checkbox] + label:after, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=radio] + span.wpcf7-list-item-label:after,
	{$rule}body .comment-form-cookies-consent input[type='checkbox'] + label:after, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=radio] + label:after,
	{$rule}body .comment-form-cookies-consent input[type='checkbox'] + label:after,
	{$rule}.woocommerce-form__input-checkbox + .woocommerce-terms-and-conditions-checkbox-text:after, 
	{$rule}.cmsmasters_notice .notice_close, 
	{$rule}.cmsmasters_quotes_slider.cmsmasters_quotes_slider_type_box .cmsmasters_quote_inner_top {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_border .cmsmasters_icon_list_item .cmsmasters_icon_list_icon:after, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner:before {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	/* Finish Primary Color */
	
	
	/* Start Highlight Color */
	{$rule}a:hover,
	{$rule}.cmsmasters_header_search_form button:hover, 
	{$rule}a.cmsmasters_cat_color:hover,
	{$rule}.cmsmasters_icon_wrap a:hover .cmsmasters_simple_icon,
	{$rule}.cmsmasters_header_search_form .cmsmasters_header_search_form_close:hover,
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_edit a:hover, 
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_meta a:hover, 
	{$rule}#page .cmsmasters_social_icon, 
	{$rule}#page .cmsmasters_profile .cmsmasters_social_icon:hover, 
	{$rule}#page .cmsmasters_open_profile .cmsmasters_social_icon:hover, 
	{$rule}.cmsmasters_likes a, 
	{$rule}.cmsmasters_likes a.active:before, 
	{$rule}.cmsmasters_likes a:hover:before, 
	{$rule}.cmsmasters_comments a, 
	{$rule}.cmsmasters_comments a:hover:before, 
	{$rule}.cmsmasters_open_post .cmsmasters_post_tags a:hover, 
	{$rule}#page .post_nav > span a:hover, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_cont_info > a:hover, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_title a:hover, 
	{$rule}.cmsmasters_archive_type .cmsmasters_archive_item_info > a:hover, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_title a:hover, 
	{$rule}.cmsmasters_post_masonry .cmsmasters_post_title a:hover, 
	{$rule}.cmsmasters_slider_post .cmsmasters_slider_post_title a:hover, 
	{$rule}.cmsmasters_post_timeline .cmsmasters_post_title a:hover,
	{$rule}.cmsmasters_profile .cmsmasters_profile_title a:hover, 
	{$rule}.cmsmasters_archive_item_title a:hover, 
	{$rule}.cmsmasters_wrap_pagination ul li p, 
	{$rule}.cmsmasters_wrap_pagination ul li .page-numbers, 
	{$rule}.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list li a, 
	{$rule}.search_bar_wrap .search_button button:hover, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_site, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_site a, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_subtitle, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_site, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_site a, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_subtitle, 
	{$rule}.cmsmasters_toggles .cmsmasters_toggles_filter a, 
	{$rule}.cmsmasters_toggles .current_toggle .cmsmasters_toggle_title, 
	{$rule}.cmsmasters_toggles .current_toggle .cmsmasters_toggle_title a, 
	{$rule}.cmsmasters_tabs .cmsmasters_tabs_list_item, 
	{$rule}.cmsmasters_tabs .cmsmasters_tabs_list_item a, 
	{$rule}#wp-calendar th, 
	{$rule}#wp-calendar td, 
	{$rule}#wp-calendar tfoot a, 
	{$rule}.widget_rss ul li a:hover, 
	{$rule}.widget_tag_cloud a:hover, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_date_wrap a, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_date_wrap a .cmsmasters_post_date,
	{$rule}.subpage_nav > span {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsmasters_notice .notice_close:hover  {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_hover']) . "
	}
	/* Finish Highlight Color */
	
	
	/* Start Headings Color */
	" . (($scheme == 'default') ? "#slide_top," : '') . "
	{$rule}h1,
	{$rule}h2,
	{$rule}h3,
	{$rule}h4,
	{$rule}h5,
	{$rule}h6,
	{$rule}h1 a,
	{$rule}h2 a,
	{$rule}h3 a,
	{$rule}h4 a,
	{$rule}h5 a,
	{$rule}h6 a,
	{$rule}fieldset legend,
	{$rule}blockquote footer,
	{$rule}blockquote, 
	{$rule}q, 
	{$rule}table caption,
	{$rule}.img_placeholder_small, 
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_horizontal .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat_title,
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap,
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat_title, 
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_title_counter_wrap, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_title, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > a,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > a,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > ul li a:before,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > a,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > ul li a:before,
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap_archive > li a:before, 
	{$rule}.cmsmasters_dropcap.type2, 
	{$rule}table thead th, 
	{$rule}table thead td, 
	{$rule}dt, 
	{$rule}.wpcf7 label, 
	{$rule}.cmsmasters-form-builder label,  
	{$rule}.comment-respond label, 
	{$rule}.cmsmasters_button, 
	{$rule}.button, 
	{$rule}input[type=submit], 
	{$rule}input[type=button], 
	{$rule}button, 
	{$rule}.cmsmasters_open_post .cmsmasters_post_date, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_date, 
	{$rule}.cmsmasters_post_timeline .cmsmasters_post_date, 
	{$rule}.cmsmasters_prev_arrow, 
	{$rule}.cmsmasters_next_arrow, 
	{$rule}.post_nav > span a, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_title, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_title a, 
	{$rule}.cmsmasters_wrap_pagination ul li .page-numbers > span, 
	{$rule}.cmsmasters_wrap_pagination ul li .page-numbers:hover p, 
	{$rule}.cmsmasters_wrap_pagination ul li a.page-numbers:hover, 
	{$rule}.cmsmasters_wrap_pagination ul li span.page-numbers, 
	{$rule}.error .button:hover, 
	{$rule}.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat_counter_wrap, 
	{$rule}.cmsmasters_pricing_table .pricing_title, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_period, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_price_wrap, 
	{$rule}.cmsmasters_pricing_table .pricing_best .feature_list a:hover,
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item .feature_list a:hover, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item.pricing_best .cmsmasters_button:hover, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_site a:hover, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_site a:hover, 
	{$rule}.cmsmasters_toggles .cmsmasters_toggles_filter a.current_filter, 
	{$rule}.cmsmasters_tabs .cmsmasters_tabs_list_item.current_tab a, 
	{$rule}#wp-calendar thead th, 
	{$rule}#wp-calendar thead td, 
	{$rule}#wp-calendar tfoot a:hover, 
	{$rule}#wp-calendar #today, 
	{$rule}.widget_custom_contact_info_entries a:hover, 	 
	{$rule}.widget_pages ul li a:hover, 
	{$rule}.widget_pages ul li.current_page_item a, 
	{$rule}.widget_nav_menu ul li a:hover, 
	{$rule}.widget_nav_menu ul li.current_page_item a, 
	{$rule}.widget_rss ul li a, 
	{$rule}.widget_tag_cloud a {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.cmsmasters_open_project .project_details_item_title, 
	{$rule}.cmsmasters_open_project .project_features_item_title, 
	{$rule}.cmsmasters_open_profile .profile_details_item_title, 
	{$rule}.cmsmasters_open_profile .profile_features_item_title {
		color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 0.9);
	}
	
	" . (($scheme == 'default') ? ".headline_outer," : '') . "
	{$rule}.cmsmasters_button:hover, 
	{$rule}.button:hover, 
	{$rule}input[type=submit]:hover, 
	{$rule}input[type=button]:hover, 
	{$rule}button:hover, 
	{$rule}.cmsmasters_icon_list_items .cmsmasters_icon_list_item .cmsmasters_icon_list_icon,
	{$rule}form .formError .formErrorContent, 
	{$rule}.cmsmasters_post_masonry .cmsmasters_post_cont:hover .cmsmasters_post_cont_img_wrap:before, 
	{$rule}.cmsmasters_slider_post .cmsmasters_slider_post_outer:hover .cmsmasters_slider_post_img_wrap:before, 
	{$rule}.cmsmasters_single_slider .cmsmasters_single_slider_item_outer:hover .cmsmasters_img_wrap:before, 
	{$rule}.cmsmasters_profile_horizontal:hover .cmsmasters_img_wrap:before, 
	{$rule}.cmsmasters_project_grid:hover .project_inner:before, 
	{$rule}.error .button, 
	{$rule}.owl-pagination .owl-page.active, 
	{$rule}.owl-pagination .owl-page:hover, 
	{$rule}.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner:before, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item.pricing_best .cmsmasters_button, 
	{$rule}.cmsmasters_toggles .current_toggle .cmsmasters_toggle_title:before, 
	{$rule}.widget_pages ul li a:hover:before, 
	{$rule}.widget_pages ul li.current_page_item a:before, 
	{$rule}.widget_nav_menu ul li a:hover:before, 
	{$rule}.widget_nav_menu ul li.current_page_item a:before {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.cmsmasters_project_puzzle .project_inner, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_inner {
		background-color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 0.85);
	}
	
	{$rule}.cmsmasters_header_search_form {
		background-color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 0.95);
	}
	
	" . (($scheme == 'default') ? ".headline_outer," : '') . "
	{$rule}.cmsmasters_button, 
	{$rule}.button, 
	{$rule}input[type=submit], 
	{$rule}input[type=button], 
	{$rule}button, 
	{$rule}.cmsmasters_project_grid:hover .project_inner, 
	{$rule}.error .button, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item.pricing_best .cmsmasters_pricing_item_inner, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item.pricing_best .cmsmasters_button, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item.pricing_best .cmsmasters_button:hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . "
	}
	/* Finish Headings Color */
	
	
	/* Start Main Background Color */
	{$rule}mark,
	{$rule}form .formError .formErrorContent,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left_top:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left:before,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_top:before,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_border .cmsmasters_icon_list_item .cmsmasters_icon_list_icon:before,
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner, 
	{$rule}.cmsmasters_button:hover, 
	{$rule}.button:hover, 
	{$rule}input[type=submit]:hover, 
	{$rule}input[type=button]:hover, 
	{$rule}button:hover {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	" . (($scheme == 'default') ? "body," : '') . "
	" . (($scheme != 'default') ? ".cmsmasters_color_scheme_{$scheme}," : '') . "
	" . (($scheme == 'default') ? ".middle_inner," : '') . "
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	{$rule}textarea,
	{$rule}option, 
	{$rule}select,
	{$rule}fieldset,
	{$rule}fieldset legend, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=checkbox] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=checkbox] + label:before, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=radio] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=radio] + label:before,
	{$rule}body .comment-form-cookies-consent input[type='checkbox'] + label:before,
	{$rule}.woocommerce-form__input-checkbox + .woocommerce-terms-and-conditions-checkbox-text:before, 
	{$rule}.cmsmasters_button, 
	{$rule}.button, 
	{$rule}input[type=submit], 
	{$rule}input[type=button], 
	{$rule}button, 
	{$rule}.about_author .about_author_inner, 
	{$rule}.cmsmasters_post_timeline .cmsmasters_post_date, 
	{$rule}.cmsmasters_project_grid, 
	{$rule}.cmsmasters_project_grid .project_inner:after, 
	{$rule}.cmsmasters_project_grid .cmsmasters_project_read_more, 
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item .cmsmasters_pricing_item_inner, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item .pricing_title {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	/* Finish Main Background Color */
	
	
	/* Start Alternate Background Color */
	" . (($scheme == 'default') ? ".headline_outer," : '') . "
	" . (($scheme == 'default') ? ".headline_outer *," : '') . "
	" . (($scheme == 'default') ? ".headline_outer a:hover," : '') . "
	{$rule}.cmsmasters_header_search_form input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]), 
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_icon_wrap, 
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_bg .cmsmasters_icon_list_item .cmsmasters_icon_list_icon:before, 
	{$rule}.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner:before, 
	{$rule}#page .cmsmasters_social_icon:hover, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_project_title a, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_project_category a:hover, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_likes a:hover, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_likes a.active, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_likes a.active:before, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_likes a:hover:before, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_comments a:hover, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_comments a:hover:before,
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_title a, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_category a:hover, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_likes a:before, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_comments a:before, 	
	{$rule}.error .button, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item.pricing_best .cmsmasters_button, 
	{$rule}.cmsmasters_notice .notice_close, 
	{$rule}.cmsmasters_header_search_form button, 
	{$rule}.cmsmasters_header_search_form .cmsmasters_header_search_form_close {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.cmsmasters_header_search_form input::-webkit-input-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.cmsmasters_header_search_form input:-moz-placeholder {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.cmsmasters_project_puzzle .cmsmasters_likes a, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_comments a, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_likes a:before, 
	{$rule}.cmsmasters_project_puzzle .cmsmasters_comments a:before, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_likes a, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_likes a.active:before, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_likes a:hover:before, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_comments a, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_comments a:hover:before {
		color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . ", 0.6);
	}
	
	{$rule}.cmsmasters_project_puzzle .cmsmasters_project_category a, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_category a {
		color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . ", 0.6);
	}
	
	{$rule}.cmsmasters_project_puzzle .cmsmasters_project_title a:hover, 
	{$rule}.cmsmasters_slider_project .cmsmasters_slider_project_title a:hover {
		color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . ", 0.7);
	}
	
	" . (($scheme == 'default') ? "#slide_top," : '') . "
	{$rule}.img_placeholder_small, 
	{$rule}.cmsmasters_featured_block,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_top,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_icon,
	{$rule}.gallery-item .gallery-icon,
	{$rule}.gallery-item .gallery-caption,
	{$rule}.cmsmasters_img.with_caption, 
	{$rule}.cmsmasters_dropcap.type2, 
	{$rule}.error .button:hover, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item.pricing_best .cmsmasters_button:hover {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.error .button:hover, 
	{$rule}.cmsmasters_img_rollover.rollover .cmsmasters_open_link:before {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . "
	}
	/* Finish Alternate Background Color */
	
	
	/* Start Borders Color */
	{$rule}blockquote:before, 
	{$rule}q:before, 
	{$rule}.cmsmasters_likes a:before, 
	{$rule}.cmsmasters_comments a:before, 
	{$rule}.cmsmasters_post_default .sticky_icon:before, 
	{$rule}#page .cmsmasters_profile .cmsmasters_social_icon, 
	{$rule}#page .cmsmasters_open_profile .cmsmasters_social_icon, 
	{$rule}.cmsmasters_twitter_wrap .twr_icon, 
	{$rule}.widget_custom_contact_info_entries > span:before, 
	{$rule}.widget_custom_contact_info_entries .adress_wrap:before, 
	{$rule}.widget_custom_twitter_entries .tweet_time:before {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_image:before, 
	{$rule}.cmsmasters_open_post .cmsmasters_post_cont_info > *:before, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_cont_info > *:before, 
	{$rule}.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list li:before, 
	{$rule}.cmsmasters_comment_item .cmsmasters_comment_item_cont_info > *:before, 
	{$rule}.cmsmasters_quotes_slider .cmsmasters_quote_subtitle_wrap > *:before, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote_subtitle_wrap > *:before, 
	{$rule}.cmsmasters_toggles .cmsmasters_toggles_filter > *:before, 
	{$rule}.cmsmasters_archive_type .cmsmasters_archive_item_info > *:before, 
	{$rule}.footer_inner:before, 
	{$rule}.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li:before, 
	{$rule}.cmsmasters_post_masonry .cmsmasters_post_cont .cmsmasters_post_cont_img_wrap:before, 
	{$rule}.cmsmasters_slider_post .cmsmasters_slider_post_outer .cmsmasters_slider_post_img_wrap:before, 
	{$rule}.cmsmasters_post_timeline .cmsmasters_post_cont .cmsmasters_post_cont_img_wrap:before, 
	{$rule}.cmsmasters_post_timeline .cmsmasters_post_cont:before, 
	{$rule}.cmsmasters_single_slider .cmsmasters_single_slider_item_outer .cmsmasters_img_wrap:before, 
	{$rule}.cmsmasters_profile_horizontal .cmsmasters_img_wrap:before, 
	{$rule}.cmsmasters_project_grid .project_inner:before, 
	{$rule}.cmsmasters_icon_list_items.cmsmasters_icon_list_type_block .cmsmasters_icon_list_item:before, 
	{$rule}.owl-pagination .owl-page, 
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_horizontal .cmsmasters_stat_wrap:before, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quotes_list:before, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quotes_vert span, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quotes_vert:before, 
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quotes_vert:after, 
	{$rule}.cmsmasters_toggles .cmsmasters_toggle_title:before, 
	{$rule}.cmsmasters_tabs.tabs_mode_tour .cmsmasters_tab_inner:before, 
	{$rule}.widget_pages ul li a:before, 
	{$rule}.widget_nav_menu ul li a:before {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.cmsmasters_quotes_grid .cmsmasters_quote, 
	{$rule}.bottom_bg, 
	{$rule}.cmsmasters_attach_img .cmsmasters_attach_img_info, 
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	{$rule}option, 
	{$rule}select,
	{$rule}fieldset,
	{$rule}fieldset legend, 
	{$rule}textarea,
	{$rule}option,
	{$rule}hr,
	{$rule}table tr, 
	{$rule}.cmsmasters_divider,
	{$rule}.cmsmasters_widget_divider,
	{$rule}.cmsmasters_img.with_caption,
	{$rule}.cmsmasters_icon_wrap .cmsmasters_simple_icon, 
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_top,
	{$rule}.cmsmasters_icon_box.cmsmasters_icon_box_left,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_icon_list_type_block .cmsmasters_icon_list_item,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_bg .cmsmasters_icon_list_icon:after,
	{$rule}.cmsmasters_icon_list_items.cmsmasters_color_type_icon .cmsmasters_icon_list_icon:after, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=checkbox] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=checkbox] + label:before, 
	{$rule}.wpcf7 form.wpcf7-form span.wpcf7-list-item input[type=radio] + span.wpcf7-list-item-label:before, 
	{$rule}.cmsmasters-form-builder .check_parent input[type=radio] + label:before,
	{$rule}body .comment-form-cookies-consent input[type='checkbox'] + label:before, 
	{$rule}.cmsmasters_open_post .cmsmasters_post_date_wrap, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_date_wrap, 
	{$rule}.cmsmasters_open_post .cmsmasters_post_content_wrap, 
	{$rule}.cmsmasters_post_default .cmsmasters_post_content_wrap, 
	{$rule}.cmsmasters_prev_arrow, 
	{$rule}.cmsmasters_next_arrow, 
	{$rule}.about_author .about_author_inner, 
	{$rule}.cmsmasters_comment_item, 
	{$rule}.cmsmasters_items_filter_wrap .cmsmasters_items_filter, 
	{$rule}.cmsmasters_project_grid .project_inner, 
	{$rule}.cmsmasters_open_project .project_sidebar, 
	{$rule}.cmsmasters_open_project .project_content.with_sidebar, 
	{$rule}.cmsmasters_open_profile .profile_sidebar, 
	{$rule}.cmsmasters_open_profile .profile_content.with_sidebar, 
	{$rule}.cmsmasters_profile_vertical	.profile_outer, 
	{$rule}.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat_title, 
	{$rule}.cmsmasters_pricing_table .cmsmasters_pricing_item .cmsmasters_pricing_item_inner, 
	{$rule}.cmsmasters_tabs.tabs_mode_tab:before, 
	{$rule}.cmsmasters_tabs.tabs_mode_tour .cmsmasters_tab_inner, 
	{$rule}.widget_custom_twitter_entries ul li {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border']) . "
	}
	/* Finish Borders Color */
	
	
	/* Start Custom Rules */
	{$rule}::selection {
		" . cmsmasters_color_css('background', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . ";
	}
	
	{$rule}::-moz-selection {
		" . cmsmasters_color_css('background', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_content_slider.cmsmasters_owl_slider .owl-buttons > div > span, 
	{$rule}.cmsmasters_open_post .cmsmasters_owl_slider .owl-buttons > div > span, 
	{$rule}.cmsmasters_post_default .cmsmasters_owl_slider .owl-buttons > div > span, 
	{$rule}.cmsmasters_open_project .cmsmasters_owl_slider .owl-buttons > div > span {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		background-color:transparent;
		box-shadow:none;
	}	

	{$rule}.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list li a {
		background-color:transparent;
		border-color:transparent;
	}
	
	{$rule}.cmsmasters_content_slider.cmsmasters_owl_slider .owl-buttons > div.owl-prev > span:before, 
	{$rule}.cmsmasters_open_post .cmsmasters_owl_slider .owl-buttons > div.owl-prev > span:before, 
	{$rule}.cmsmasters_post_default .cmsmasters_owl_slider .owl-buttons > div.owl-prev > span:before, 
	{$rule}.cmsmasters_open_project .cmsmasters_owl_slider .owl-buttons > div.owl-prev > span:before {
		background: -webkit-linear-gradient(to right, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
		background: -moz-linear-gradient(to right, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
		background: -ms-linear-gradient(to right, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
		background: -o-linear-gradient(to right, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
		background: linear-gradient(to right, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
	}
	
	{$rule}.cmsmasters_content_slider.cmsmasters_owl_slider .owl-buttons > div.owl-next > span:before, 
	{$rule}.cmsmasters_open_post .cmsmasters_owl_slider .owl-buttons > div.owl-next > span:before, 
	{$rule}.cmsmasters_open_project .cmsmasters_owl_slider .owl-buttons > div.owl-next > span:before, 
	{$rule}.cmsmasters_post_default .cmsmasters_owl_slider .owl-buttons > div.owl-next > span:before {
		background: -webkit-linear-gradient(to left, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
		background: -moz-linear-gradient(to left, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
		background: -ms-linear-gradient(to left, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
		background: -o-linear-gradient(to left, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
		background: linear-gradient(to left, rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 1) 0%, rgba(0,0,0,0) 100%);
	}
	
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	{$rule}textarea,
	{$rule}select,
	{$rule}.cmsmasters_prev_arrow, 
	{$rule}.cmsmasters_next_arrow {
		-webkit-box-shadow:inset 0 0 0 0 " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border'] . ";
		box-shadow:inset 0 0 0 0 " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border'] . ";
	}
	
	" . (($scheme == 'default') ? "#slide_top, " : '') . "
	{$rule}input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]):focus,
	{$rule}textarea:focus, 
	{$rule}select:focus, 
	{$rule}.post_nav > span a:hover + span, 
	{$rule}.cmsmasters_prev_arrow:hover, 
	{$rule}.cmsmasters_next_arrow:hover, 
	{$rule}.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner, 
	{$rule}.cmsmasters_wrap_pagination ul li .page-numbers > span {
		-webkit-box-shadow:inset 0 0 0 1px " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border'] . ";
		box-shadow:inset 0 0 0 1px " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border'] . ";
	}
	
	" . (($scheme == 'default') ? "#slide_top:hover, " : '') . "
	{$rule}.cmsmasters_wrap_pagination ul li .page-numbers:hover > span {
		-webkit-box-shadow:inset 0 0 0 2px " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border'] . ";
		box-shadow:inset 0 0 0 2px " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border'] . ";
	}
	";
	
	
	if ($scheme != 'default') {
		$custom_css .= "
		.cmsmasters_color_scheme_{$scheme}.cmsmasters_row_top_zigzag:before, 
		.cmsmasters_color_scheme_{$scheme}.cmsmasters_row_bot_zigzag:after {
			background-image: -webkit-linear-gradient(135deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					-webkit-linear-gradient(45deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
			background-image: -moz-linear-gradient(135deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					-moz-linear-gradient(45deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
			background-image: -ms-linear-gradient(135deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					-ms-linear-gradient(45deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
			background-image: -o-linear-gradient(135deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					-o-linear-gradient(45deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
			background-image: linear-gradient(315deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%), 
					linear-gradient(45deg, " . $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg'] . " 25%, transparent 25%);
		}
		";
	}
	
	
	$custom_css .= "
	/* Finish Custom Rules */

/***************** Finish {$title} Color Scheme Rules ******************/


/***************** Start {$title} Button Color Scheme Rules ******************/
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_hover:hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_bd_underline {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_bd_underline:hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_left, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_right, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_top, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_bottom, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_vert, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_hor, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_diag {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_left:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_right:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_top:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_bottom:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_vert:hover, 
	{$rule}.cmsmasters_button.cm.sms_but_bg_expand_hor:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_diag:hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_left:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_right:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_top:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_slide_bottom:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_vert:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_hor:after, 
	{$rule}.cmsmasters_button.cmsmasters_but_bg_expand_diag:after {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_shadow {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_shadow:hover {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_dark_bg, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_light_bg, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_divider {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_dark_bg:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_light_bg:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_divider:hover {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_divider:after {
		" . cmsmasters_color_css('border-right-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:before {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:after {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:hover:before {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_inverse:hover:after {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_slide_left, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_slide_right {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_slide_left:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_slide_right:hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_left, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_right, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_top, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_bottom {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_left:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_right:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_top:hover, 
	{$rule}.cmsmasters_button.cmsmasters_but_icon_hover_slide_bottom:hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}

/***************** Finish {$title} Button Color Scheme Rules ******************/


";
	}
	
	
	return apply_filters('galleria_metropolia_theme_colors_primary_filter', $custom_css);
}

