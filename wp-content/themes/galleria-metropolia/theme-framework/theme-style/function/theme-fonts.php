<?php
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version 	1.0.6
 * 
 * Theme Fonts Rules
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_theme_fonts() {
	$cmsmasters_option = galleria_metropolia_get_global_options();
	
	
	$custom_css = "/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version 	1.0.6
 * 
 * Theme Fonts Rules
 * Created by CMSMasters
 * 
 */


/***************** Start Theme Font Styles ******************/

	/* Start Content Font */
	body, 
	table thead th, 
	table thead td, 
	.footer_copyright, 
	.cmsmasters_open_post .cmsmasters_post_author a, 
	.cmsmasters_open_post .cmsmasters_post_content,	
	.cmsmasters_post_default .cmsmasters_post_author a, 
	.cmsmasters_post_default .cmsmasters_post_content, 
	.cmsmasters_post_masonry .cmsmasters_post_author a, 
	.cmsmasters_post_masonry .cmsmasters_post_date, 
	.cmsmasters_post_masonry .cmsmasters_post_content, 
	.cmsmasters_post_timeline .cmsmasters_post_author a, 
	.cmsmasters_post_timeline .cmsmasters_post_content, 
	.cmsmasters_slider_post .cmsmasters_slider_post_author a, 
	.cmsmasters_slider_post .cmsmasters_slider_post_date, 
	.cmsmasters_slider_post .cmsmasters_slider_post_content, 
	.cmsmasters_open_post .cmsmasters_post_tags, 
	.cmsmasters_open_post .cmsmasters_post_tags a, 
	.cmsmasters_open_project .project_details_item_desc, 
	.cmsmasters_open_project .project_features_item_desc, 
	.cmsmasters_open_project .project_details_item_desc a, 
	.cmsmasters_open_project .project_features_item_desc a, 
	.cmsmasters_open_profile .profile_details_item_desc, 
	.cmsmasters_open_profile .profile_features_item_desc, 
	.cmsmasters_open_profile .profile_details_item_desc a, 
	.cmsmasters_open_profile .profile_features_item_desc a, 
	.cmsmasters_archive_item_date_wrap, 
	.cmsmasters_pricing_table .pricing_best .feature_list, 
	.cmsmasters_pricing_table .pricing_best .feature_list a, 
	.cmsmasters_notice .notice_content, 
	.cmsmasters_notice .notice_content a, 
	.widget_recent_comments ul li, 
	.widget_custom_contact_info_entries, 
	.widget_custom_contact_info_entries a, 
	.widget_rss ul li .rss-date, 
	.widget_rss ul li .rssSummary,
	#wp-calendar td,
	#wp-calendar td a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_content_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_content_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_style'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_user_name, 
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_user_name a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_content_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_content_font_system_font'] . ";
	}
	
	.footer_copyright, 
	.widget_custom_contact_info_entries, 
	.widget_custom_contact_info_entries a, 
	.widget_rss ul li .rss-date, 
	.widget_rss ul li .rssSummary {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] - 2) . "px;
	}
	
	.cmsmasters_open_post .cmsmasters_post_tags, 
	.cmsmasters_open_post .cmsmasters_post_tags a, 
	.cmsmasters_open_project .project_details_item_desc, 
	.cmsmasters_open_project .project_features_item_desc, 
	.cmsmasters_open_project .project_details_item_desc a, 
	.cmsmasters_open_project .project_features_item_desc a, 
	.cmsmasters_open_profile .profile_details_item_desc, 
	.cmsmasters_open_profile .profile_features_item_desc, 
	.cmsmasters_open_profile .profile_details_item_desc a, 
	.cmsmasters_open_profile .profile_features_item_desc a, 
	.cmsmasters_archive_item_date_wrap, 
	.widget_recent_comments ul li, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_lpr_tabs_cont > .published {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] - 4) . "px;
	}
	
	#page .cmsmasters_open_project .project_details_item_title, 
	#page .cmsmasters_open_project .project_features_item_title, 
	#page .cmsmasters_open_profile .profile_details_item_title, 
	#page .cmsmasters_open_profile .profile_features_item_title {
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] - 4) . "px;
	}
	
	.cmsmasters_icon_list_items li:before {
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] . "px;
	}
	
	.cmsmasters_open_post .cmsmasters_post_author a, 
	.cmsmasters_post_default .cmsmasters_post_author a, 
	.cmsmasters_post_masonry .cmsmasters_post_author a, 
	.cmsmasters_slider_post .cmsmasters_slider_post_author a, 
	.cmsmasters_post_timeline .cmsmasters_post_author a, 
	.cmsmasters_post_masonry .cmsmasters_post_date, 
	.cmsmasters_slider_post .cmsmasters_slider_post_date {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] - 2) . "px;
		font-weight:normal;
	}
	
	.cmsmasters_open_post .cmsmasters_post_content, 
	.cmsmasters_post_default .cmsmasters_post_content, 
	.cmsmasters_open_project .cmsmasters_project_content, 
	.cmsmasters_open_profile .cmsmasters_profile_content {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] + 1) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] + 2) . "px;
	}
	/* Finish Content Font */


	/* Start Link Font */
	a, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_lpr_tabs_cont > a, 
	.cmsmasters_single_slider .cmsmasters_single_slider_item_title, 
	.cmsmasters_single_slider .cmsmasters_single_slider_item_title a, 
	.cmsmasters_comment_item .cmsmasters_comment_item_cont_info, 
	.cmsmasters_comment_item .cmsmasters_comment_item_cont_info a,
	.subpage_nav > strong,
	.subpage_nav > span,
	.subpage_nav > a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_link_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_link_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_link_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_link_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_link_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	a:hover {
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_link_hover_decoration'] . ";
	}
	
	.cmsmasters_comment_item .cmsmasters_comment_item_cont_info, 
	.cmsmasters_comment_item .cmsmasters_comment_item_cont_info a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] - 2) . "px;
	}
	
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_lpr_tabs_cont > a {
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_link_font_line_height'] - 4) . "px;
	}
	/* Finish Link Font */


	/* Start Navigation Title Font */
	.navigation > li > a, 
	.top_line_nav > li > a, 
	.header_top .meta_wrap, 
	.header_top .meta_wrap a, 
	ul.navigation .nav_tag, 
	nav > div > ul div.menu-item-mega-container > ul > li > a, 
	.footer_nav > li > a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_nav_title_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_nav_title_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_nav_title_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_nav_title_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_nav_title_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_nav_title_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_nav_title_font_text_transform'] . ";
		letter-spacing:0.1em;
	}
	
	.top_line_nav > li > a, 
	.header_top .meta_wrap, 
	.header_top .meta_wrap a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_nav_title_font_font_size'] - 1) . "px;
	}
	
	.cmsmasters_footer_small .footer_nav > li > a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_nav_title_font_font_size'] + 1) . "px;
	}
	
	.footer_nav > li > a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_nav_title_font_font_size'] + 3) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_nav_title_font_line_height'] + 2) . "px;
		font-weight:700;
		text-transform:none;
	}
	
	ul.navigation .nav_tag {
		font-size:9px;
		line-height:16px;
	}
	/* Finish Navigation Title Font */


	/* Start Navigation Dropdown Font */	
	.navigation ul li a,
	.top_line_nav ul li a, 
	ul.navigation .nav_subtitle {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_text_transform'] . ";
		letter-spacing:0;
	}
	
	ul.navigation .nav_subtitle {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_font_size'] - 2) . "px;
	}
	
	@media only screen and (max-width: 1024px) {
		.navigation > li > a, 
		.top_line_nav > li > a {
			font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_system_font'] . ";
			font-size:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['galleria-metropolia' . '_nav_dropdown_font_text_transform'] . ";
			letter-spacing:0;
		}
	}
	/* Finish Navigation Dropdown Font */


	/* Start H1 Font */
	h1,
	h1 a,
	.logo .title, 
	.cmsmasters_post_timeline .cmsmasters_post_date span.cmsmasters_day, 
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap, 
	.cmsmasters_pricing_table .cmsmasters_price_wrap, 
	.cmsmasters_header_search_form input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]) {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h1_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h1_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h1_font_font_size'] - 8) . "px;
	}
	
	.cmsmasters_pricing_table .cmsmasters_price_wrap {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h1_font_font_size'] - 10) . "px;
	}
	
	.cmsmasters_dropcap {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h1_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h1_font_system_font'] . ";
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_icon_list_items.cmsmasters_icon_list_icon_type_number .cmsmasters_icon_list_item .cmsmasters_icon_list_icon:before,
	.cmsmasters_icon_box.box_icon_type_number:before,
	.cmsmasters_icon_box.cmsmasters_icon_heading_left.box_icon_type_number .icon_box_heading:before {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h1_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h1_font_system_font'] . ";
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_style'] . ";
	}
	
	.cmsmasters_header_search_form input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]) {
		font-size:60px; /* static */
	}
	
	@media only screen and (max-width: 768px) {
		font-size:35px; /* static */
	}
	
	.cmsmasters_dropcap.type1 {
		font-size:52px; /* static */
	}
	
	.cmsmasters_dropcap.type2 {
		font-size:35px; /* static */
	}
	/* Finish H1 Font */


	/* Start H2 Font */
	h2,
	h2 a,
	.cmsmasters_open_post .cmsmasters_post_date span.cmsmasters_day, 
	.cmsmasters_post_default .cmsmasters_post_date span.cmsmasters_day, 
	.cmsmasters_sitemap_wrap h1, 
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > a, 
	.cmsmasters_archive_item_title, 
	.cmsmasters_archive_item_title a, 
	.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h2_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h2_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_archive_item_title, 
	.cmsmasters_archive_item_title a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h2_font_font_size'] - 4) . "px;
	}
	
	.headline_outer .headline_inner .headline_icon:before {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h2_font_font_size'] + 6) . "px;
	}
	/* Finish H2 Font */


	/* Start H3 Font */
	h3,
	h3 a, 
	.cmsmasters_tabs .cmsmasters_tabs_list_item, 
	.cmsmasters_tabs .cmsmasters_tabs_list_item a, 
	.cmsmasters_open_project .project_details_title, 
	.cmsmasters_open_project .project_features_title, 
	.cmsmasters_open_profile .profile_details_title, 
	.cmsmasters_open_profile .profile_features_title, 
	.cmsmasters_toggles .cmsmasters_toggle_title a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h3_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h3_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_open_project .project_details_title, 
	.cmsmasters_open_project .project_features_title, 
	.cmsmasters_open_profile .profile_details_title, 
	.cmsmasters_open_profile .profile_features_title {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] + 2) . "px;
	}
	
	@media only screen and (max-width: 768px) {
		#page .cmsmasters_archive_item_title, 
		#page .cmsmasters_archive_item_title a, 
		#page .share_posts_title, 
		#page .about_author_title, 
		#page .cmsmasters_single_slider_title {
			font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h3_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h3_font_system_font'] . ";
			font-size:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_transform'] . ";
			text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_decoration'] . ";
			letter-spacing:0;
		}
		
		.headline_outer .entry-title {
			font-size:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_line_height'] . "px;
		}
		
		.headline_outer .headline_inner .headline_icon:before {
			font-size:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] . "px;
		}
	}
	/* Finish H3 Font */


	/* Start H4 Font */
	h4, 
	h4 a, 
	.post_nav a, 
	.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat_counter_wrap, 
	.cmsmasters_stats .cmsmasters_stat_wrap .cmsmasters_stat_title, 
	.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap, 
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > a, 
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > a, 
	#wp-calendar caption, 
	.widget_rss ul li a	{
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h4_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h4_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_stats.stats_mode_bars.stats_type_horizontal .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner:before {
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_line_height'] . "px;
	}
	
	#wp-calendar caption {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_line_height'] - 6) . "px;
	}
	
	.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat_counter_wrap {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_font_size'] - 4) . "px;
	}
	
	.cmsmasters_stats.stats_mode_bars.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat {
		padding-top:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_line_height'] * 2 + 13) . "px;
	}
	
	.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat_container {
		height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_line_height'] * 2 + 220 + 13) . "px;
	}
	
	.widget_tag_cloud a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h4_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h4_font_system_font'] . ";
		font-weight:normal; /* static */
	} 
	/* Finish H4 Font */


	/* Start H5 Font */
	h5,
	h5 a, 
	dt, 
	.headline_outer .headline_inner .entry-subtitle, 
	.cmsmasters_breadcrumbs .cmsmasters_breadcrumbs_inner, 
	.cmsmasters_breadcrumbs .cmsmasters_breadcrumbs_inner a, 
	.cmsmasters_open_post .cmsmasters_post_date, 
	.cmsmasters_open_post .cmsmasters_post_category, 
	.cmsmasters_open_post .cmsmasters_post_category a, 
	.cmsmasters_post_default .cmsmasters_post_date, 
	.cmsmasters_post_default .cmsmasters_post_category, 
	.cmsmasters_post_default .cmsmasters_post_category a, 
	.cmsmasters_post_masonry .cmsmasters_post_category, 
	.cmsmasters_post_masonry .cmsmasters_post_category a, 	
	.cmsmasters_post_timeline .cmsmasters_post_category, 
	.cmsmasters_post_timeline .cmsmasters_post_category a, 
	.cmsmasters_post_timeline .cmsmasters_post_date, 
	.cmsmasters_slider_post .cmsmasters_slider_post_category, 
	.cmsmasters_slider_post .cmsmasters_slider_post_category a, 	
	.cmsmasters_project_puzzle .cmsmasters_project_category, 
	.cmsmasters_project_puzzle .cmsmasters_project_category a, 
	.cmsmasters_slider_project .cmsmasters_slider_project_category, 
	.cmsmasters_slider_project .cmsmasters_slider_project_category a, 
	.cmsmasters_wrap_pagination ul li p, 
	.cmsmasters_wrap_pagination ul li .page-numbers, 
	.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list li a, 
	.cmsmasters_project_grid .cmsmasters_project_category, 
	.cmsmasters_project_grid .cmsmasters_project_category a, 
	.cmsmasters_open_project .project_details_item_title, 
	.cmsmasters_open_project .project_features_item_title, 
	.cmsmasters_open_profile .profile_details_item_title, 
	.cmsmasters_open_profile .profile_features_item_title, 
	.cmsmasters_archive_type .cmsmasters_archive_item_info, 
	.cmsmasters_archive_type .cmsmasters_archive_item_info a, 
	.cmsmasters_pricing_table .pricing_title, 
	.cmsmasters_twitter_wrap .published, 
	.cmsmasters_quotes_grid .cmsmasters_quote_site, 
	.cmsmasters_quotes_grid .cmsmasters_quote_site a, 
	.cmsmasters_quotes_grid .cmsmasters_quote_subtitle, 
	.cmsmasters_quotes_slider .cmsmasters_quote_site, 
	.cmsmasters_quotes_slider .cmsmasters_quote_site a, 
	.cmsmasters_quotes_slider .cmsmasters_quote_subtitle, 
	.cmsmasters_toggles .cmsmasters_toggles_filter, 
	.cmsmasters_toggles .cmsmasters_toggles_filter a, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tabs_list_item, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tabs_list_item a, 
	#wp-calendar table th, 
	#wp-calendar table td, 
	#wp-calendar tfoot a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h5_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h5_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_decoration'] . ";
		letter-spacing:0.1em;
	}
	
	.wpcf7 label, 
	.cmsmasters-form-builder label, 
	#wp-calendar tfoot a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h5_font_font_size'] - 2) . "px;
	}
	
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_user_name, 
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_user_name a {
		text-transform:none;
		letter-spacing:0;
	}
	
	table thead th, 
	table thead td {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h5_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h5_font_system_font'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.error_subtitle, 
	dt {
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h5_font_line_height'] + 6) . "px;
	}
	
	@media only screen and (max-width: 768px) {
		.cmsmasters_open_post .cmsmasters_post_date_wrap .cmsmasters_post_date span, 
		.cmsmasters_post_default .cmsmasters_post_date_wrap .cmsmasters_post_date span {
			font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h5_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h5_font_system_font'] . ";
			font-size:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_transform'] . ";
			text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_decoration'] . ";
			letter-spacing:0;
		}
	}
	/* Finish H5 Font */


	/* Start H6 Font */
	h6,
	h6 a, 
	.wpcf7 label, 
	.cmsmasters-form-builder label, 
	.comment-respond label, 
	.cmsmasters_comment_item .cmsmasters_comment_item_cont_info > a, 
	.cmsmasters_post_default .cmsmasters_post_read_more, 
	.cmsmasters_post_masonry .cmsmasters_post_read_more, 
	.cmsmasters_post_timeline .cmsmasters_post_read_more, 
	.cmsmasters_slider_post .cmsmasters_slider_post_read_more, 
	.cmsmasters_project_grid .cmsmasters_project_read_more, 
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_title, 
	.cmsmasters_pricing_table .cmsmasters_period, 
	.widget_pages ul li a, 
	.widget_nav_menu ul li a, 
	.widget_custom_twitter_entries .tweet_time {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h6_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_text_decoration'] . ";
		letter-spacing:0.1em;
	}
	
	.widget_pages ul li a, 
	.widget_nav_menu ul li a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h6_font_font_size'] - 1) . "px; 
	}
	
	.wpcf7 label, 
	.cmsmasters-form-builder label, 
	.comment-respond label {
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h5_font_line_height'] + 10) . "px; 
	}
	
	.cmsmasters_project_grid .cmsmasters_project_read_more {
		font-size:12px; /* static */
		line-height:26px; /* static */
		height:26px; /* static */
	}
	
	.cmsmasters_likes a, 
	.cmsmasters_comments a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h6_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h6_font_system_font'] . ";
	}
	
	
	/* Finish H6 Font */


	/* Start Button Font */
	.cmsmasters_button, 
	.button, 
	input[type=submit], 
	input[type=button], 
	button {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_button_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_button_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_button_font_text_transform'] . ";
		letter-spacing:0.1em;
	}
	
	.gform_wrapper .gform_footer input.button, 
	.gform_wrapper .gform_footer input[type=submit] {
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_size'] . "px !important;
	}
	
	.cmsmasters_button.cmsmasters_but_icon_dark_bg, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg, 
	.cmsmasters_button.cmsmasters_but_icon_divider, 
	.cmsmasters_button.cmsmasters_but_icon_inverse {
		padding-left:" . ((int) $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] + 20) . "px;
	}
	
	.cmsmasters_button.cmsmasters_but_icon_dark_bg:before, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg:before, 
	.cmsmasters_button.cmsmasters_but_icon_divider:before, 
	.cmsmasters_button.cmsmasters_but_icon_inverse:before, 
	.cmsmasters_button.cmsmasters_but_icon_dark_bg:after, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg:after, 
	.cmsmasters_button.cmsmasters_but_icon_divider:after, 
	.cmsmasters_button.cmsmasters_but_icon_inverse:after {
		width:" . $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] . "px;
	}
	
	.cmsmasters_items_filter_wrap .cmsmasters_items_sort_but {
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] - 12) . "px; 
	}
	/* Finish Button Font */


	/* Start Small Text Font */
	small, 
	form .formError .formErrorContent, 
	.wpcf7-list-item-label, 
	body .cmsmasters-form-builder .check_parent input[type='checkbox'] + label, 
	body .cmsmasters-form-builder .check_parent input[type='radio'] + label	{
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_small_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_small_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_small_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_small_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_small_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_small_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_small_font_text_transform'] . ";
		letter-spacing:0;
	}
	
	.gform_wrapper .description, 
	.gform_wrapper .gfield_description, 
	.gform_wrapper .gsection_description, 
	.gform_wrapper .instruction {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_small_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_small_font_system_font'] . " !important;
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_small_font_font_size'] . "px !important;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_small_font_line_height'] . "px !important;
		letter-spacing:0;
	}
	/* Finish Small Text Font */


	/* Start Text Fields Font */
	.wp-caption .wp-caption-text, input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	textarea,
	select,
	option,
	code {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_input_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_input_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_input_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_input_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_input_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_input_font_font_style'] . ";
		letter-spacing:0;
	}

	code {
		letter-spacing:1px;
	}
	
	.wp-caption .wp-caption-text, 
	.wp-caption .wp-caption-text a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_input_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_input_font_font_size'] + 2) . "px;
	}
	
	.gform_wrapper input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]),
	.gform_wrapper textarea, 
	.gform_wrapper select {
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_input_font_font_size'] . "px !important;
	}
	/* Finish Text Fields Font */


	/* Start Blockquote Font */
	blockquote , 
	blockquote a, 
	blockquote code, 
	q,
	q * {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_quote_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_quote_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_quote_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_quote_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_quote_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_quote_font_font_style'] . ";
		letter-spacing:0;
	}

	blockquote a,
	blockquote a > * {
		font-weight:600;
	}
	/* Finish Blockquote Font */

/***************** Finish Theme Font Styles ******************/


";
	
	
	return apply_filters('galleria_metropolia_theme_fonts_filter', $custom_css);
}

