<?php 
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version 	1.0.0
 * 
 * Admin Panel Element Options
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_options_element_tabs() {
	$tabs = array();
	
	$tabs['sidebar'] = esc_attr__('Sidebars', 'galleria-metropolia');
	
	if (class_exists('Cmsmasters_Content_Composer')) {
		$tabs['icon'] = esc_attr__('Social Icons', 'galleria-metropolia');
	}
	
	$tabs['lightbox'] = esc_attr__('Lightbox', 'galleria-metropolia');
	$tabs['sitemap'] = esc_attr__('Sitemap', 'galleria-metropolia');
	$tabs['error'] = esc_attr__('404', 'galleria-metropolia');
	$tabs['code'] = esc_attr__('Custom Codes', 'galleria-metropolia');
	
	if (class_exists('Cmsmasters_Form_Builder')) {
		$tabs['recaptcha'] = esc_attr__('reCAPTCHA', 'galleria-metropolia');
	}
	
	return apply_filters('cmsmasters_options_element_tabs_filter', $tabs);
}


function galleria_metropolia_options_element_sections() {
	$tab = galleria_metropolia_get_the_tab();
	
	switch ($tab) {
	case 'sidebar':
		$sections = array();
		
		$sections['sidebar_section'] = esc_attr__('Custom Sidebars', 'galleria-metropolia');
		
		break;
	case 'icon':
		$sections = array();
		
		$sections['icon_section'] = esc_attr__('Social Icons', 'galleria-metropolia');
		
		break;
	case 'lightbox':
		$sections = array();
		
		$sections['lightbox_section'] = esc_attr__('Theme Lightbox Options', 'galleria-metropolia');
		
		break;
	case 'sitemap':
		$sections = array();
		
		$sections['sitemap_section'] = esc_attr__('Sitemap Page Options', 'galleria-metropolia');
		
		break;
	case 'error':
		$sections = array();
		
		$sections['error_section'] = esc_attr__('404 Error Page Options', 'galleria-metropolia');
		
		break;
	case 'code':
		$sections = array();
		
		$sections['code_section'] = esc_attr__('Custom Codes', 'galleria-metropolia');
		
		break;
	case 'recaptcha':
		$sections = array();
		
		$sections['recaptcha_section'] = esc_attr__('Form Builder Plugin reCAPTCHA Keys', 'galleria-metropolia');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return apply_filters('cmsmasters_options_element_sections_filter', $sections, $tab);	
} 


function galleria_metropolia_options_element_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = galleria_metropolia_get_the_tab();
	}
	
	
	$options = array();
	
	
	$defaults = galleria_metropolia_settings_element_defaults();
	
	
	switch ($tab) {
	case 'sidebar':
		$options[] = array( 
			'section' => 'sidebar_section', 
			'id' => 'galleria-metropolia' . '_sidebar', 
			'title' => esc_html__('Custom Sidebars', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'sidebar', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_sidebar'] 
		);
		
		break;
	case 'icon':
		$options[] = array( 
			'section' => 'icon_section', 
			'id' => 'galleria-metropolia' . '_social_icons', 
			'title' => esc_html__('Social Icons', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'social', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_social_icons'] 
		);
		
		break;
	case 'lightbox':
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_skin', 
			'title' => esc_html__('Skin', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_skin'], 
			'choices' => array( 
				esc_html__('Dark', 'galleria-metropolia') . '|dark', 
				esc_html__('Light', 'galleria-metropolia') . '|light', 
				esc_html__('Mac', 'galleria-metropolia') . '|mac', 
				esc_html__('Metro Black', 'galleria-metropolia') . '|metro-black', 
				esc_html__('Metro White', 'galleria-metropolia') . '|metro-white', 
				esc_html__('Parade', 'galleria-metropolia') . '|parade', 
				esc_html__('Smooth', 'galleria-metropolia') . '|smooth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_path', 
			'title' => esc_html__('Path', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets path for switching windows', 'galleria-metropolia'), 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_path'], 
			'choices' => array( 
				esc_html__('Vertical', 'galleria-metropolia') . '|vertical', 
				esc_html__('Horizontal', 'galleria-metropolia') . '|horizontal' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_infinite', 
			'title' => esc_html__('Infinite', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the ability to infinite the group', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_infinite'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_aspect_ratio', 
			'title' => esc_html__('Keep Aspect Ratio', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the resizing method used to keep aspect ratio within the viewport', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_aspect_ratio'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_mobile_optimizer', 
			'title' => esc_html__('Mobile Optimizer', 'galleria-metropolia'), 
			'desc' => esc_html__('Make lightboxes optimized for giving better experience with mobile devices', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_mobile_optimizer'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_max_scale', 
			'title' => esc_html__('Max Scale', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the maximum viewport scale of the content', 'galleria-metropolia'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_max_scale'], 
			'min' => 0.1, 
			'max' => 2, 
			'step' => 0.05 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_min_scale', 
			'title' => esc_html__('Min Scale', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the minimum viewport scale of the content', 'galleria-metropolia'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_min_scale'], 
			'min' => 0.1, 
			'max' => 2, 
			'step' => 0.05 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_inner_toolbar', 
			'title' => esc_html__('Inner Toolbar', 'galleria-metropolia'), 
			'desc' => esc_html__('Bring buttons into windows, or let them be over the overlay', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_inner_toolbar'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_smart_recognition', 
			'title' => esc_html__('Smart Recognition', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets content auto recognize from web pages', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_smart_recognition'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_fullscreen_one_slide', 
			'title' => esc_html__('Fullscreen One Slide', 'galleria-metropolia'), 
			'desc' => esc_html__('Decide to fullscreen only one slide or hole gallery the fullscreen mode', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_fullscreen_one_slide'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_fullscreen_viewport', 
			'title' => esc_html__('Fullscreen Viewport', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the resizing method used to fit content within the fullscreen mode', 'galleria-metropolia'), 
			'type' => 'select', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_fullscreen_viewport'], 
			'choices' => array( 
				esc_html__('Center', 'galleria-metropolia') . '|center', 
				esc_html__('Fit', 'galleria-metropolia') . '|fit', 
				esc_html__('Fill', 'galleria-metropolia') . '|fill', 
				esc_html__('Stretch', 'galleria-metropolia') . '|stretch' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_controls_toolbar', 
			'title' => esc_html__('Toolbar Controls', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets buttons be available or not', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_controls_toolbar'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_controls_arrows', 
			'title' => esc_html__('Arrow Controls', 'galleria-metropolia'), 
			'desc' => esc_html__('Enable the arrow buttons', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_controls_arrows'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_controls_fullscreen', 
			'title' => esc_html__('Fullscreen Controls', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the fullscreen button', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_controls_fullscreen'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_controls_thumbnail', 
			'title' => esc_html__('Thumbnails Controls', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the thumbnail navigation', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_controls_thumbnail'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_controls_keyboard', 
			'title' => esc_html__('Keyboard Controls', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the keyboard navigation', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_controls_keyboard'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_controls_mousewheel', 
			'title' => esc_html__('Mouse Wheel Controls', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the mousewheel navigation', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_controls_mousewheel'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_controls_swipe', 
			'title' => esc_html__('Swipe Controls', 'galleria-metropolia'), 
			'desc' => esc_html__('Sets the swipe navigation', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_controls_swipe'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'galleria-metropolia' . '_ilightbox_controls_slideshow', 
			'title' => esc_html__('Slideshow Controls', 'galleria-metropolia'), 
			'desc' => esc_html__('Enable the slideshow feature and button', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_ilightbox_controls_slideshow'] 
		);
		
		break;
	case 'sitemap':
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'galleria-metropolia' . '_sitemap_nav', 
			'title' => esc_html__('Website Pages', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_sitemap_nav'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'galleria-metropolia' . '_sitemap_categs', 
			'title' => esc_html__('Blog Archives by Categories', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_sitemap_categs'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'galleria-metropolia' . '_sitemap_tags', 
			'title' => esc_html__('Blog Archives by Tags', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_sitemap_tags'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'galleria-metropolia' . '_sitemap_month', 
			'title' => esc_html__('Blog Archives by Month', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_sitemap_month'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'galleria-metropolia' . '_sitemap_pj_categs', 
			'title' => esc_html__('Portfolio Archives by Categories', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_sitemap_pj_categs'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'galleria-metropolia' . '_sitemap_pj_tags', 
			'title' => esc_html__('Portfolio Archives by Tags', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_sitemap_pj_tags'] 
		);
		
		break;
	case 'error':
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_color', 
			'title' => esc_html__('Text Color', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_color'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_bg_color', 
			'title' => esc_html__('Background Color', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_bg_color'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_bg_img_enable', 
			'title' => esc_html__('Background Image Visibility', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_bg_img_enable'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_bg_image', 
			'title' => esc_html__('Background Image', 'galleria-metropolia'), 
			'desc' => esc_html__('Choose your custom error page background image.', 'galleria-metropolia'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_bg_image'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_bg_rep', 
			'title' => esc_html__('Background Repeat', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_bg_rep'], 
			'choices' => array( 
				esc_html__('No Repeat', 'galleria-metropolia') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'galleria-metropolia') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'galleria-metropolia') . '|repeat-y', 
				esc_html__('Repeat', 'galleria-metropolia') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_bg_pos', 
			'title' => esc_html__('Background Position', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_bg_pos'], 
			'choices' => array( 
				esc_html__('Top Left', 'galleria-metropolia') . '|top left', 
				esc_html__('Top Center', 'galleria-metropolia') . '|top center', 
				esc_html__('Top Right', 'galleria-metropolia') . '|top right', 
				esc_html__('Center Left', 'galleria-metropolia') . '|center left', 
				esc_html__('Center Center', 'galleria-metropolia') . '|center center', 
				esc_html__('Center Right', 'galleria-metropolia') . '|center right', 
				esc_html__('Bottom Left', 'galleria-metropolia') . '|bottom left', 
				esc_html__('Bottom Center', 'galleria-metropolia') . '|bottom center', 
				esc_html__('Bottom Right', 'galleria-metropolia') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_bg_att', 
			'title' => esc_html__('Background Attachment', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_bg_att'], 
			'choices' => array( 
				esc_html__('Scroll', 'galleria-metropolia') . '|scroll', 
				esc_html__('Fixed', 'galleria-metropolia') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_bg_size', 
			'title' => esc_html__('Background Size', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'galleria-metropolia') . '|auto', 
				esc_html__('Cover', 'galleria-metropolia') . '|cover', 
				esc_html__('Contain', 'galleria-metropolia') . '|contain' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_search', 
			'title' => esc_html__('Search Line', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_search'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_sitemap_button', 
			'title' => esc_html__('Sitemap Button', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_sitemap_button'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'galleria-metropolia' . '_error_sitemap_link', 
			'title' => esc_html__('Sitemap Page URL', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_error_sitemap_link'], 
			'class' => '' 
		);
		
		break;
	case 'code':
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'galleria-metropolia' . '_custom_css', 
			'title' => esc_html__('Custom CSS', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_custom_css'], 
			'class' => 'allowlinebreaks' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'galleria-metropolia' . '_custom_js', 
			'title' => esc_html__('Custom JavaScript', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_custom_js'], 
			'class' => 'allowlinebreaks' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'galleria-metropolia' . '_gmap_api_key', 
			'title' => esc_html__('Google Maps API key', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_gmap_api_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'galleria-metropolia' . '_api_key', 
			'title' => esc_html__('Twitter API key', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_api_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'galleria-metropolia' . '_api_secret', 
			'title' => esc_html__('Twitter API secret', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_api_secret'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'galleria-metropolia' . '_access_token', 
			'title' => esc_html__('Twitter Access token', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_access_token'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'galleria-metropolia' . '_access_token_secret', 
			'title' => esc_html__('Twitter Access token secret', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_access_token_secret'], 
			'class' => '' 
		);
		
		break;
	case 'recaptcha':
		$options[] = array( 
			'section' => 'recaptcha_section', 
			'id' => 'galleria-metropolia' . '_recaptcha_public_key', 
			'title' => esc_html__('reCAPTCHA Public Key', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_recaptcha_public_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'recaptcha_section', 
			'id' => 'galleria-metropolia' . '_recaptcha_private_key', 
			'title' => esc_html__('reCAPTCHA Private Key', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_recaptcha_private_key'], 
			'class' => '' 
		);
		
		break;
	}
	
	return apply_filters('cmsmasters_options_element_fields_filter', $options, $tab);	
}

