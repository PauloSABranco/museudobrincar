<?php
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version		1.0.0
 * 
 * Tribe Events Fonts Rules
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_tribe_events_fonts($custom_css) {
	$cmsmasters_option = galleria_metropolia_get_global_options();
	
	
	$custom_css .= "
/***************** Start Tribe Events Font Styles ******************/

	/* Start Content Font */
	.tribe-events-tooltip,
	.tribe-events-tooltip a,
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info,
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info a,
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info,
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info a
	.tribe-events-list .tribe-events-event-meta, 
	.tribe-events-list .tribe-events-event-meta a, 
	.tribe-mini-calendar tbody, 
	.tribe-mini-calendar tbody a, 
	table.tribe-events-calendar tbody td .tribe-events-month-event-title, 
	table.tribe-events-calendar tbody td .tribe-events-month-event-title a, 
	.tribe-events-tooltip .tribe-events-event-body, 
	.tribe-events-grid .tribe-week-event .vevent .entry-title, 
	.tribe-events-grid .tribe-week-event .vevent .entry-title a, 
	.tribe-events-photo .tribe-events-event-meta, 
	.tribe-events-photo .tribe-events-event-meta a, 
	.tribe-events-photo .tribe-events-list-photo-description, 
	.cmsmasters_single_event .tribe-events-cost, 
	.cmsmasters_single_event .tribe-events-schedule, 
	.cmsmasters_single_event .tribe-events-schedule a, 
	.tribe-events-organizer  .organizer-address, 
	.tribe-events-organizer  .organizer-address a, 
	.cmsmasters_single_event .cmsmasters_single_event_header_right, 
	.cmsmasters_single_event .cmsmasters_single_event_header_right a, 
	.tribe-events-organizer .cmsmasters_events_organizer_header_right, 
	.tribe-events-organizer .cmsmasters_events_organizer_header_right a, 
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item .cmsmasters_event_meta_info_item_descr, 
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item .cmsmasters_event_meta_info_item_descr a, 
	.tribe-events-venue .tribe-events-event-meta, 
	.tribe-events-venue .tribe-events-event-meta a, 
	.tribe-events-organizer .tribe-events-event-meta, 
	.tribe-events-organizer .tribe-events-event-meta a, 
	.tribe-events-venue .cmsmasters_events_venue_header_right a, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info a, 
	.tribe-events-venue-widget .tribe-venue-widget-venue-name a, 
	.widget .vcalendar .cmsmasters_widget_event_info, 
	.widget .vcalendar .cmsmasters_widget_event_info a, 
	.tribe-events-venue-widget .vcalendar .cmsmasters_widget_event_info, 
	.tribe-events-venue-widget .vcalendar .cmsmasters_widget_event_info a, 
	.tribe-this-week-events-widget .tribe-this-week-event .duration, 
	.tribe-this-week-events-widget .tribe-this-week-event .tribe-venue,
	.tribe-this-week-events-widget .tribe-this-week-event .tribe-venue a,
	.tribe-this-week-events-widget .tribe-events-page-title {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_content_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_content_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_style'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info,
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info a,
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info,
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info a
	.tribe-events-photo .tribe-events-list-photo-description, 
	.tribe-events-photo .tribe-events-event-meta, 
	.tribe-events-photo .tribe-events-event-meta a, 
	.tribe-events-list .tribe-events-event-meta, 
	.tribe-events-list .tribe-events-event-meta a {
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] - 4) . "px;
	}
	
	.tribe-mini-calendar tbody, 
	.tribe-mini-calendar tbody a, 
	.tribe-mini-calendar thead th, 
	.tribe-events-tooltip .description, 
	.tribe-events-widget-link a, 
	.tribe-this-week-events-widget .tribe-events-viewmore a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] - 6) . "px;
	}
	
	.tribe-this-week-events-widget .tribe-this-week-event .duration, 
	.tribe-this-week-events-widget .tribe-this-week-event .tribe-venue,
	.tribe-this-week-events-widget .tribe-this-week-event .tribe-venue a,
	.tribe-this-week-events-widget .tribe-events-page-title, 
	.tribe-events-venue-widget .vcalendar .cmsmasters_widget_event_info, 
	.tribe-events-venue-widget .vcalendar .cmsmasters_widget_event_info a, 
	.widget .vcalendar .cmsmasters_widget_event_info, 
	.widget .vcalendar .cmsmasters_widget_event_info a, 
	.tribe-events-venue-widget .tribe-venue-widget-venue-name a, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info a, 
	.tribe-events-tooltip .tribe-events-event-body {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] - 3) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] - 6) . "px;
	}
	
	.tribe-events-organizer .tribe-events-event-meta, 
	.tribe-events-organizer .tribe-events-event-meta a, 
	.tribe-events-venue .tribe-events-event-meta, 
	.tribe-events-venue .tribe-events-event-meta a, 
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item .cmsmasters_event_meta_info_item_descr, 
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item .cmsmasters_event_meta_info_item_descr a, 
	.cmsmasters_single_event .cmsmasters_single_event_header_right, 
	.cmsmasters_single_event .cmsmasters_single_event_header_right a, 
	.tribe-events-organizer .cmsmasters_events_organizer_header_right, 
	.tribe-events-organizer .cmsmasters_events_organizer_header_right a, 
	.cmsmasters_single_event .tribe-events-cost, 
	.cmsmasters_single_event .tribe-events-schedule, 
	.cmsmasters_single_event .tribe-events-schedule a, 
	.tribe-events-organizer  .organizer-address, 
	.tribe-events-organizer  .organizer-address a, 
	.tribe-events-grid .tribe-week-event .vevent .entry-title, 
	.tribe-events-grid .tribe-week-event .vevent .entry-title a, 
	table.tribe-events-calendar tbody td .tribe-events-month-event-title, 
	table.tribe-events-calendar tbody td .tribe-events-month-event-title a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] - 8) . "px;
	}
	
	.tribe-events-venue-widget .vcalendar .cmsmasters_widget_event_info, 
	.tribe-events-venue-widget .vcalendar .cmsmasters_widget_event_info a, 
	.widget .vcalendar .cmsmasters_widget_event_info, 
	.widget .vcalendar .cmsmasters_widget_event_info a, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info a, 
	.tribe-events-organizer .tribe-events-event-meta, 
	.tribe-events-organizer .tribe-events-event-meta a, 
	.tribe-events-venue .tribe-events-event-meta, 
	.tribe-events-venue .tribe-events-event-meta a, 
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item .cmsmasters_event_meta_info_item_descr, 
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item .cmsmasters_event_meta_info_item_descr a,
	.tribe-events-grid .tribe-week-event .vevent .entry-title, 
	.tribe-events-grid .tribe-week-event .vevent .entry-title a {
		text-transform:none;
	}
	/* Finish Content Font */
	
	
	/* Start Link Font */
		#tribe-events-content > .tribe-events-button {
			font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_link_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_link_font_system_font'] . ";
			font-size:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['galleria-metropolia' . '_link_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['galleria-metropolia' . '_link_font_text_transform'] . ";
			text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_link_font_text_decoration'] . ";
			letter-spacing:0;
		}
	/* Finish Link Font */
	
	
	/* Start H1 Font */
	.cmsmasters_event_big_day {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h1_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h1_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h1_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_event_big_day {
		font-size:60px; /* static */
		line-height:60px; /* static */
	}
	
	.tribe_mini_calendar_widget .cmsmasters_event_big_date {
		font-size:30px; /* static */
		line-height:30px; /* static */
	}
	/* Finish H1 Font */
	
	
	/* Start H2 Font */
	.tribe-events-countdown-widget .tribe-countdown-time, 
	.tribe-events-related-events-title,
	.tribe-events-page-title {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h2_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h2_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.tribe-events-countdown-widget .tribe-countdown-time {
		font-size:30px;
		line-height:40px;
	}
	
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-countdown-widget .tribe-countdown-time {
		font-size:40px;
		line-height:40px;
	}
	
	@media only screen and (min-width: 1440px) {
		.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-countdown-widget .tribe-countdown-time {
			font-size:60px;
			line-height:60px;
		}
	}
	/* Finish H2 Font */
	
	
	/* Start H3 Font */	
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .entry-title a,
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-list-widget .tribe-events-list-widget-content-wrap .entry-title a,
	.tribe-mobile-day .tribe-mobile-day-date,
	.tribe-events-single-section-title, 
	.tribe-events-list-event-title, 
	.tribe-events-list-event-title a, 
	.cmsmasters_event_big_month {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h3_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h3_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.cmsmasters_event_big_month, 
	.tribe-events-related-events-title {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] - 2) . "px;
	}
	
	.tribe-events-single-section-title {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] + 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h3_font_line_height'] - 4) . "px;
	}
	
	@media only screen and (max-width: 768px) {
		#page .tribe-events-page-title {
			font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h3_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h3_font_system_font'] . ";
			font-size:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_transform'] . ";
			text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_decoration'] . ";
			letter-spacing:0;
		}
	}
	/* Finish H3 Font */
	
	
	/* Start H4 Font */
	.widget .vcalendar .entry-title, 
	.widget .vcalendar .entry-title a 
	.tribe-this-week-events-widget .tribe-this-week-event .entry-title,
	.tribe-this-week-events-widget .tribe-this-week-event .entry-title a,
	.tribe-events-countdown-widget .tribe-countdown-text, 
	.tribe-events-countdown-widget .tribe-countdown-text a,
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .entry-title, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .entry-title a, 
	.tribe_mini_calendar_widget .cmsmasters_event_big_month, 
	.tribe-events-tooltip .entry-title, 
	#page .tribe-events-venue-widget .vcalendar .entry-title, 
	#page .tribe-events-venue-widget .vcalendar .entry-title a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h4_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h4_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h4_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.tribe-this-week-events-widget .tribe-this-week-event .entry-title,
	.tribe-this-week-events-widget .tribe-this-week-event .entry-title a, 
	.tribe-events-tooltip .entry-title {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_font_size'] - 2) . "px;
	}
	
	.widget .vcalendar .entry-title, 
	.widget .vcalendar .entry-title a, 
	#page .tribe-events-venue-widget .vcalendar .entry-title, 
	#page .tribe-events-venue-widget .vcalendar .entry-title a, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .entry-title, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .entry-title a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_line_height'] - 6) . "px;
	}
	
	.tribe-events-countdown-widget .tribe-countdown-text, 
	.tribe-events-countdown-widget .tribe-countdown-text a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_font_size'] - 3) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h4_font_line_height'] - 6) . "px;
	}
	/* Finish H4 Font */
	
	
	/* Start H5 Font */
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item, 
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item a, 
	.cmsmasters_event_big_week,
	.tribe-mini-calendar [id*=tribe-mini-calendar-month], 
	.tribe-events-list .tribe-events-day-time-slot > h5, 
	.tribe-events-list .tribe-events-list-separator-month,
	.tribe-mobile-day .tribe-events-read-more {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h5_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h5_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_decoration'] . ";
		letter-spacing:0.1em;
	}
	
	.tribe-mini-calendar-list-wrapper .entry-title,
	.tribe-mini-calendar-list-wrapper .entry-title a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h5_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h5_font_line_height'] - 4) . "px;
	}
	/* Finish H5 Font */
	
	
	/* Start H6 Font */
	.tribe-this-week-events-widget .tribe-this-week-widget-header-date, 
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-under, 
	.tribe-events-grid .column.first > span,
	.tribe-events-grid .tribe-week-grid-hours div,
	table.tribe-events-calendar tbody td div[id*=tribe-events-daynum-], 
	table.tribe-events-calendar tbody td div[id*=tribe-events-daynum-] a, 
	.tribe-bar-filters-inner > div label, 
	.tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info, 
	.tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info a, 
	table.tribe-events-calendar tbody td .tribe-events-viewmore a,
	.tribe-mobile-day .tribe-events-event-schedule-details, 
	.tribe-mobile-day .tribe-event-schedule-details {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h6_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_text_decoration'] . ";
		letter-spacing:0.1em;
	}
	
	.tribe-this-week-events-widget .tribe-this-week-widget-header-date, 
	.tribe-events-countdown-widget .tribe-countdown-time .tribe-countdown-under {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h6_font_font_size'] - 2) . "px;
	}
	
	.tribe-events-grid .column.first > span,
	.tribe-events-grid .tribe-week-grid-hours div {
		font-style:11px; /* static */
		line-height:16px; /* static */
	}
	
	.tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info, 
	.tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info a, 
	.tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info, 
	.tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info a, 
	table.tribe-events-calendar tbody td .tribe-events-viewmore a,
	ul.tribe-related-events .tribe-related-event-info .published {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h6_font_font_size'] - 2) . "px;
	}
	
	.cmsmasters_sidebar.sidebar_layout_11 .tribe-events-countdown-widget .tribe-countdown-text a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h6_font_font_size'] + 10) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h6_font_line_height'] + 10) . "px;
	}
	
	table.tribe-events-calendar tbody td div[id*=tribe-events-daynum-], 
	table.tribe-events-calendar tbody td div[id*=tribe-events-daynum-] a {
		letter-spacing:0;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h6_font_line_height'] + 4) . "px;
	}
	/* Finish H6 Font */
	
	
	/* Start Button Font */
	.tribe-events-grid .tribe-grid-header, 
	.tribe-events-grid .tribe-grid-header a, 
	table.tribe-events-calendar thead th, 
	#tribe-bar-views label, 
	#tribe-bar-views .tribe-bar-views-list li, 
	#tribe-bar-views .tribe-bar-views-list li a {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_button_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_button_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_button_font_text_transform'] . ";
		letter-spacing:0.1em;
	}
	
	.one_first .cmsmasters_sidebar.sidebar_layout_11 .tribe-events-list-widget .cmsmasters_events_more .button, .one_first .cmsmasters_sidebar.sidebar_layout_11 .tribe-events-adv-list-widget .cmsmasters_events_more .button {
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] - 4) . "px; 
	}
	
	#tribe-bar-views label, 
	#tribe-bar-views .tribe-bar-views-list li, 
	#tribe-bar-views .tribe-bar-views-list li a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_button_font_font_size'] + 2) . "px;
	}
	
	.tribe-events-grid .tribe-grid-header, 
	.tribe-events-grid .tribe-grid-header a, 
	table.tribe-events-calendar thead th {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_button_font_font_size'] + 2) . "px;
		line-height:22px;
	}
	/* Finish Button Font */
	
	
	/* Start Small Text Font */
	/* Finish Small Text Font */

/***************** Finish Tribe Events Font Styles ******************/

";
	
	
	return $custom_css;
}

add_filter('galleria_metropolia_theme_fonts_filter', 'galleria_metropolia_tribe_events_fonts');

