<?php 
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version		1.0.0
 * 
 * Theme Admin Settings
 * Created by CMSMasters
 * 
 */


/* Color Settings */
function galleria_metropolia_theme_options_color_fields($options, $tab) {
	$defaults = galleria_metropolia_color_schemes_defaults();

	if ($tab == 'header') {
		$options[] = array( 
			'section' => $tab . '_section', 
			'id' => 'galleria-metropolia' . '_' . $tab . '_overlaps_bg', 
			'title' => esc_html__('Header Background Color for Overlap', 'galleria-metropolia'), 
			'desc' => esc_html__('(if header overlaps content)', 'galleria-metropolia'), 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['overlaps_bg'] 
		);
	}
	
	
	return $options;
}

add_filter('cmsmasters_options_color_fields_filter', 'galleria_metropolia_theme_options_color_fields', 10, 2);



/* Single Settings */
function galleria_metropolia_theme_options_single_fields($options, $tab) {
	$defaults = galleria_metropolia_settings_single_defaults();
	
	$options_new = array();
	
	if ($tab == 'post') {
		foreach ($options as $option) {
			if ($option['id'] == 'galleria-metropolia' . '_blog_post_cat') {
				$options_new[] = array( 
					'section' => 'post_section', 
					'id' => 'galleria-metropolia' . '_blog_post_date_settings', 
					'title' => esc_html__('Use Date Settings from WordPress General Settings', 'galleria-metropolia'), 
					'desc' => esc_html__('show', 'galleria-metropolia'), 
					'type' => 'checkbox', 
					'std' => $defaults[$tab]['galleria-metropolia' . '_blog_post_date_settings'] 
				);
				
				$options_new[] = $option;
			} else {
				$options_new[] = $option;
			}
		}
	} else {
		$options_new = $options;
	}
	
	
	return $options_new;
}

add_filter('cmsmasters_options_single_fields_filter', 'galleria_metropolia_theme_options_single_fields', 10, 2);