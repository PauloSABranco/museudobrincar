<?php
/**
 * A configuração de base do WordPress
 *
 * Este ficheiro define os seguintes parâmetros: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, e ABSPATH. Pode obter mais informação
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} no Codex. As definições de MySQL são-lhe fornecidas pelo seu serviço de alojamento.
 *
 * Este ficheiro contém as seguintes configurações:
 *
 * * Configurações de  MySQL
 * * Chaves secretas
 * * Prefixo das tabelas da base de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Definições de MySQL - obtenha estes dados do seu serviço de alojamento** //
/** O nome da base de dados do WordPress */
define( 'DB_NAME', 'museubri_museu' );

/** O nome do utilizador de MySQL */
define( 'DB_USER', 'root' );

/** A password do utilizador de MySQL  */
define( 'DB_PASSWORD', '' );

/** O nome do serviddor de  MySQL  */
define( 'DB_HOST', 'localhost' );

/** O "Database Charset" a usar na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O "Database Collate type". Se tem dúvidas não mude. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação.
 *
 * Mude para frases únicas e diferentes!
 * Pode gerar frases automáticamente em {@link https://api.wordpress.org/secret-key/1.1/salt/ Serviço de chaves secretas de WordPress.org}
 * Pode mudar estes valores em qualquer altura para invalidar todos os cookies existentes o que terá como resultado obrigar todos os utilizadores a voltarem a fazer login
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'V{22pJyoKp!gU9K!5BC~`_[B#YF=6nJrG)2Kf{K){K6R)=Qnt5R#|L8AD4P=1Y*0' );
define( 'SECURE_AUTH_KEY',  'VS(%r/cW+L]*YsiQh$xC8l.XO O2/O}X d+Y3TvpFp~$c8Hl@S%jI(6Dgo+T8$.l' );
define( 'LOGGED_IN_KEY',    'dFWR*d|lCY$s?[3]]IBOw+Vfw`gNR~7Z7sBi873s7@=siKY}^,Wl*,(yW;>Jo>g+' );
define( 'NONCE_KEY',        'hU!{rBpLW<%/1Gd$l,(58CN^8`P{b[yARr|^Ca<lqK6]?Z.S]nI){x%<!LO:QqzG' );
define( 'AUTH_SALT',        '?y|G7^DqVY!IM&*<Mg1]V)8(:27J-hB}BwVZNku76lU.R51g 5nt3&~/kt+@`uaa' );
define( 'SECURE_AUTH_SALT', '.O)bz=8paMzNglf_lgtaZhv>Nm,&[-v|_UJYi1>Q#hl9HMFN@B21m^/1F*R0qx-M' );
define( 'LOGGED_IN_SALT',   '4mIY@J/C/@NtvC=Xi$,6hS&vM(Yt8`2/B@[aIvtn<tU`R$JLxh9)*pEAuR#J^^Y=' );
define( 'NONCE_SALT',       'G#xpb~YHqdUjsH%^6r&smtalejc3%-0F;~&`u04/2CAopfL5/`33ls8~8cziP;jM' );

/**#@-*/

/**
 * Prefixo das tabelas de WordPress.
 *
 * Pode suportar múltiplas instalações numa só base de dados, ao dar a cada
 * instalação um prefixo único. Só algarismos, letras e underscores, por favor!
 */
$table_prefix = 'wp_';

/**
 * Para developers: WordPress em modo debugging.
 *
 * Mude isto para true para mostrar avisos enquanto estiver a testar.
 * É vivamente recomendado aos autores de temas e plugins usarem WP_DEBUG
 * no seu ambiente de desenvolvimento.
 *
 * Para mais informações sobre outras constantes que pode usar para debugging,
 * visite o Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* E é tudo. Pare de editar! */

/** Caminho absoluto para a pasta do WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Define as variáveis do WordPress e ficheiros a incluir. */
require_once(ABSPATH . 'wp-settings.php');
