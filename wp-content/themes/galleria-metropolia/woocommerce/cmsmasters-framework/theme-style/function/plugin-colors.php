<?php
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version		1.0.1
 * 
 * WooCommerce Colors Rules
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_woocommerce_colors($custom_css) {
	$cmsmasters_option = galleria_metropolia_get_global_options();
	
	
	$cmsmasters_color_schemes = cmsmasters_color_schemes_list();
	
	
	foreach ($cmsmasters_color_schemes as $scheme => $title) {
		$rule = (($scheme != 'default') ? "html .cmsmasters_color_scheme_{$scheme} " : '');
		
		
		$custom_css .= "
/***************** Start {$title} WooCommerce Color Scheme Rules ******************/

	/* Start Main Content Font Color */
	{$rule}.cmsmasters_product .price del, 
	{$rule}.widget > .product_list_widget del .amount, 
	{$rule}.select2-container .select2-choice, 
	{$rule}.select2-container.select2-drop-above .select2-choice, 
	{$rule}.select2-container.select2-container-active .select2-choice, 
	{$rule}.select2-container.select2-container-active.select2-drop-above .select2-choice, 
	{$rule}.select2-drop.select2-drop-active, 
	{$rule}.select2-drop.select2-drop-above.select2-drop-active, 
	{$rule}.woocommerce-order-pay .shop_table .cart-subtotal td, 
	{$rule}.woocommerce-order-pay .shop_table .order-total td, 	
	{$rule}.shop_table.woocommerce-checkout-review-order-table .cart-subtotal td, 
	{$rule}.shop_table.woocommerce-checkout-review-order-table .order-total td, 	
	{$rule}.woocommerce-order-pay .shop_table .cart-subtotal td, 
	{$rule}.woocommerce-order-pay .shop_table .order-total td, 
	{$rule}.widget_layered_nav ul li a:hover, 
	{$rule}.widget_layered_nav ul li.chosen a, 
	{$rule}.widget_layered_nav_filters ul li a:hover, 
	{$rule}.widget_layered_nav_filters ul li.chosen a, 
	{$rule}.widget_product_categories ul li a:hover, 
	{$rule}.widget_product_categories ul li.current-cat a, 
	{$rule}.widget_price_filter .price_slider_amount .price_label, 
	{$rule}.widget_shopping_cart .cart_list .quantity, 
	{$rule}.widget_shopping_cart .cart_list .quantity .amount, 
	{$rule}.woocommerce-store-notice .woocommerce-store-notice__dismiss-link, 
	{$rule}.widget_shopping_cart .total, 
	{$rule}.widget > .product_list_widget .amount, 
	{$rule}.cmsmasters_single_product .product_meta a {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_color']) . "
	}
	/* Finish Main Content Font Color */
	
	
	/* Start Primary Color */
	{$rule}.cmsmasters_dynamic_cart.cmsmasters_active .cmsmasters_dynamic_cart_button, 
	{$rule}.shop_table td.product-subtotal .amount, 
	{$rule}.required, 
	{$rule}.cmsmasters_star_rating .cmsmasters_star_color_wrap, 
	{$rule}.comment-form-rating .stars > span a:hover, 
	{$rule}.comment-form-rating .stars > span a.active, 
	{$rule}#page .remove:hover, 
	{$rule}.cmsmasters_product .price ins, 
	{$rule}.cmsmasters_single_product .price ins, 
	{$rule}.shop_table .product-name a, 
	{$rule}.woocommerce-order-pay .shop_table .product-name strong, 
	{$rule}.woocommerce-order-pay .shop_table .product-name strong, 
	{$rule}.shop_table.woocommerce-checkout-review-order-table .product-name strong, 
	{$rule}.shop_table.order_details tfoot tr:last-child th, 
	{$rule}.shop_table.order_details tfoot tr:last-child td, 
	{$rule}.shop_table.order_details .product-name strong, 
	{$rule}.shop_table.order_details tfoot tr:first-child th, 
	{$rule}.shop_table.order_details tfoot tr:first-child td,  
	{$rule}.cmsmasters_product_cat, 
	{$rule}.cmsmasters_product_cat a, 
	{$rule}.widget_layered_nav ul li, 
	{$rule}.widget_layered_nav ul li a, 
	{$rule}.widget_layered_nav_filters ul li, 
	{$rule}.widget_layered_nav_filters ul li a, 
	{$rule}.widget_product_categories ul li, 
	{$rule}.widget_product_categories ul li a {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.input-checkbox + label:after, 
	{$rule}.input-radio + label:after, 
	{$rule}input.shipping_method + label:after, 
	{$rule}.widget_price_filter .ui-slider-range {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.select2-container.select2-container-active .select2-choice, 
	{$rule}.select2-container.select2-container-active.select2-drop-above .select2-choice, 
	{$rule}.select2-drop.select2-drop-active, 
	{$rule}.select2-drop.select2-drop-above.select2-drop-active {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_link']) . "
	}
	/* Finish Primary Color */
	
	
	/* Start Highlight Color */
	{$rule}.woocommerce-MyAccount-navigation ul > li.is-active a, 
	{$rule}.select2-container .select2-selection--single .select2-selection__rendered, 
	{$rule}.widget_product_categories ul li:before,
	{$rule}.cmsmasters_product .button_to_cart, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .cart_list a:hover, 
	{$rule}.onsale span, 
	{$rule}.out-of-stock span, 
	{$rule}.stock span, 
	{$rule}.cmsmasters_product_cat a:hover, 
	{$rule}.woocommerce-loop-category__title:hover, 
	{$rule}.cmsmasters_product .cmsmasters_product_title a:hover, 
	{$rule}#page .shop_table .remove, 
	{$rule}.shop_table .product-name a:hover, 
	{$rule}.form-row label a:hover, 
	{$rule}.widget > .product_list_widget a:hover, 
	{$rule}.widget_shopping_cart .cart_list a:hover {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.cmsmasters_dynamic_cart:hover .cmsmasters_dynamic_cart_button {
	" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_hover']) . "
	}
	
	{$rule}.link_hover_color {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_hover']) . "
	}
	/* Finish Highlight Color */
	
	
	/* Start Headings Color */
	{$rule}.cmsmasters_product .button_to_cart:hover, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .buttons .button, 
	{$rule}.cmsmasters_single_product .product_meta a:hover, 
	{$rule}.woocommerce-order-pay .shop_table .order-total th, 
	{$rule}.woocommerce-order-pay .shop_table .cart-subtotal th, 	
	{$rule}.shop_table.woocommerce-checkout-review-order-table .order-total th, 
	{$rule}.shop_table.woocommerce-checkout-review-order-table .cart-subtotal th, 
	{$rule}.shop_table thead th, 
	{$rule}.woocommerce-loop-category__title, 
	{$rule}.cmsmasters_product .cmsmasters_product_title a, 
	{$rule}.cmsmasters_product_cat a, 
	{$rule}.woocommerce-info, 
	{$rule}.woocommerce-message, 
	{$rule}.woocommerce-error li, 
	{$rule}#page .remove, 
	{$rule}.cmsmasters_woo_wrap_result .woocommerce-result-count, 
	{$rule}.cmsmasters_product .cmsmasters_product_cat, 
	{$rule}.cmsmasters_product .price, 
	{$rule}.shop_attributes th, 
	{$rule}ul.order_details strong, 
	{$rule}.widget > .product_list_widget a, 
	{$rule}.widget_shopping_cart .cart_list a, 
	{$rule}#page .shop_table .remove:hover, 
	{$rule}.shop_table td.product-subtotal > .amount, 
	{$rule}.cart_totals table .cart-subtotal th, 
	{$rule}.cart_totals table .order-total th, 
	{$rule}.form-row label,
	{$rule}.form-row label a, 
	{$rule}.cmsmasters_single_product .onsale, 
	{$rule}.cmsmasters_single_product .out-of-stock, 
	{$rule}.cmsmasters_single_product .stock, 
	{$rule}.woocommerce-store-notice {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.cmsmasters_dynamic_cart .cmsmasters_dynamic_cart_button, 
	{$rule}.out-of-stock span, 
	{$rule}.stock span, 
	{$rule}.widget_price_filter .ui-slider-handle:before, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .buttons .button:hover, 
	{$rule}.cmsmasters_product:hover .cmsmasters_product_inner:before, 
	{$rule}.cmsmasters_product_placeholder {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .buttons .button:hover, 
	{$rule}.cmsmasters_added_product_info {
		background-color:rgba(" . cmsmasters_color2rgb($cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . ", 0.9);
	}
	
	{$rule}.cmsmasters_added_product_info, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .buttons .button:hover {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_heading']) . "
	}
	/* Finish Headings Color */
	
	
	/* Start Main Background Color */
	{$rule}.cmsmasters_dynamic_cart .cmsmasters_dynamic_cart_button, 
	{$rule}.cmsmasters_dynamic_cart .cmsmasters_dynamic_cart_button:hover, 
	{$rule}.cmsmasters_dynamic_cart:hover .cmsmasters_dynamic_cart_button, 
	{$rule}.onsale, 
	{$rule}.out-of-stock, 
	{$rule}.stock {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.select2-container.select2-drop-above .select2-choice, 
	{$rule}.select2-container.select2-container-active .select2-choice, 
	{$rule}.select2-container.select2-container-active.select2-drop-above .select2-choice, 
	{$rule}.select2-drop.select2-drop-active, 
	{$rule}.select2-drop.select2-drop-above.select2-drop-active, 
	{$rule}.input-checkbox + label:before, 
	{$rule}.input-radio + label:before, 
	{$rule}input.shipping_method + label:before, 
	{$rule}.woocommerce-info, 
	{$rule}.woocommerce-message, 
	{$rule}.woocommerce-error, 
	{$rule}.woocommerce-order-pay .shop_table .cart-subtotal,
	{$rule}.shop_table.woocommerce-checkout-review-order-table .cart-subtotal,
	{$rule}.cart_totals table .cart-subtotal, 
	{$rule}.cart_totals table .order-total, 
	{$rule}.shop_table thead th, 
	{$rule}.shop_table .actions, 
	{$rule}.woocommerce-order-pay .shop_table .order-total th, 
	{$rule}.woocommerce-order-pay .shop_table .order-total td, 	
	{$rule}.shop_table.woocommerce-checkout-review-order-table .order-total th, 
	{$rule}.shop_table.woocommerce-checkout-review-order-table .order-total td, 
	{$rule}.shop_table.order_details tfoot tr:last-child th, 
	{$rule}.shop_table.order_details tfoot tr:last-child td, 
	{$rule}.select2-container .select2-selection--single, 
	{$rule}.select2-container .select2-choice {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_bg']) . "
	}
	/* Finish Main Background Color */
	
	
	/* Start Alternate Background Color */
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .buttons .button:hover, 
	{$rule}#page .cmsmasters_dynamic_cart .remove, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .cart_list a, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .cart_list .quantity, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .cart_list .variation, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .cart_list .variation dt, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .cart_list .variation dd, 
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .total, 
	{$rule}.cmsmasters_added_product_info {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .buttons .button, 
	{$rule}.onsale span, 
	{$rule}.out-of-stock span, 
	{$rule}.stock span, 
	{$rule}.woocommerce-store-notice {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . "
	}
	
	{$rule}.cmsmasters_dynamic_cart .widget_shopping_cart_content .buttons .button {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_alternate']) . "
	}
	/* Finish Alternate Background Color */
	
	
	/* Start Borders Color */
	{$rule}.woocommerce-store-notice p a, 
	{$rule}.woocommerce-store-notice p a:hover, 
	{$rule}.cmsmasters_star_rating .cmsmasters_star_trans_wrap, 
	{$rule}.comment-form-rating .stars > span {
		" . cmsmasters_color_css('color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.woocommerce-checkout-payment, 
	{$rule}.shop_table td, 
	{$rule}.shop_table th, 
	{$rule}.woocommerce-message, 
	{$rule}.woocommerce-info, 
	{$rule}.woocommerce-error, 
	{$rule}.shop_attributes tr, 
	{$rule}.select2-container .select2-choice, 
	{$rule}.select2-container.select2-drop-above .select2-choice, 
	{$rule}.input-checkbox + label:before, 
	{$rule}.input-radio + label:before,
	{$rule}.woocommerce-form__input-checkbox + .woocommerce-terms-and-conditions-checkbox-text:before, 
	{$rule}input.shipping_method + label:before, 
	{$rule}.cart_totals table th, 
	{$rule}.cart_totals table td, 
	{$rule}.widget_price_filter .price_slider, 
	{$rule}.shop_table .cart_item,
	{$rule}.cmsmasters_dynamic_cart_wrap, 
	{$rule}.select2-dropdown,
	{$rule}.select2-container .select2-selection--single, 
	{$rule}.select2-container.select2-container--open .select2-selection--single,
	{$rule}.select2-container.select2-container--focus .select2-selection--single, 
	{$rule}.woocommerce-checkout-payment .payment_methods .payment_box, 
	{$rule}ul.order_details, 
	{$rule}ul.order_details li, 
	{$rule}ul.order_details strong {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.cmsmasters_product .cmsmasters_product_inner:before, 
	{$rule}.widget_price_filter .price_slider, 
	{$rule}.woocommerce-store-notice .woocommerce-store-notice__dismiss-link {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['galleria-metropolia' . '_' . $scheme . '_border']) . "
	}
	/* Finish Borders Color */

/***************** Finish {$title} WooCommerce Color Scheme Rules ******************/

";
	}
	
	
	return $custom_css;
}

add_filter('galleria_metropolia_theme_colors_secondary_filter', 'galleria_metropolia_woocommerce_colors');

