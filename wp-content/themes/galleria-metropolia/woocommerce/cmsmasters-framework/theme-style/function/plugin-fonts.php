<?php
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version 	1.0.1
 * 
 * WooCommerce Fonts Rules
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_woocommerce_fonts($custom_css) {
	$cmsmasters_option = galleria_metropolia_get_global_options();
	
	
	$custom_css .= "
/***************** Start WooCommerce Font Styles ******************/
	
	/* Start Content Font */
	.shop_table .product-name a, 
	.woocommerce-order-pay .shop_table  .product-name dl, 
	.shop_table.woocommerce-checkout-review-order-table .product-name dl, 
	.shop_table.order_details .product-name dl, 
	.shop_table td > .amount, 
	.cart_totals table td, 
	.cart_totals table strong, 
	.woocommerce-order-pay .shop_table .product-name strong, 
	.woocommerce-order-pay .shop_table .order-total td, 
	.woocommerce-order-pay .shop_table .order-total td .amount, 
	.woocommerce-order-pay .shop_table .cart-subtotal td, 
	.woocommerce-order-pay .shop_table .cart-subtotal td .amount, 
	.shop_table.woocommerce-checkout-review-order-table .product-name strong, 
	.shop_table.woocommerce-checkout-review-order-table .order-total td, 
	.shop_table.woocommerce-checkout-review-order-table .order-total td .amount, 
	.shop_table.woocommerce-checkout-review-order-table .cart-subtotal td, 
	.shop_table.woocommerce-checkout-review-order-table .cart-subtotal td .amount, 
	.shop_attributes td	{
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_content_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_content_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_content_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_content_font_font_style'] . ";
		letter-spacing:0;
	}
	
	.woocommerce-order-pay .shop_table .product-name strong, 
	.shop_table.woocommerce-checkout-review-order-table .product-name strong {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_content_font_font_size'] - 3) . "px;
	}
	
	.woocommerce-order-pay .shop_table .product-name dl, 
	.shop_table.woocommerce-checkout-review-order-table .product-name dl, 
	.shop_table.order_details .product-name dl {
		text-transform:none;
	}
	/* Finish Content Font */
	
	
	/* Start Link Font */
	.widget_shopping_cart_content .cart_list a, 
	.widget > .product_list_widget a, 
	.woocommerce-order-pay .shop_table .product-name, 
	.shop_table.woocommerce-checkout-review-order-table .product-name, 
	.cart_totals table th, 
	.woocommerce-order-pay .shop_table .cart-subtotal th, 
	.woocommerce-order-pay .shop_table .order-total th, 	
	.shop_table.woocommerce-checkout-review-order-table .cart-subtotal th, 
	.shop_table.woocommerce-checkout-review-order-table .order-total th, 
	.cmsmasters_single_product .product_meta * {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_link_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_link_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_link_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_link_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_link_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_link_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.widget_shopping_cart_content .cart_list a, 
	.widget > .product_list_widget a {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_link_font_font_size'] - 1) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_link_font_line_height'] - 4) . "px;
	}
	
	.cmsmasters_single_product .product_meta > span * {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_link_font_font_size'] - 2) . "px;
		font-weight:normal; /* static */
	}
	/* Finish Link Font */
	
	
	/* Start H1 Font */
	/* Finish H1 Font */
	
	
	/* Start H2 Font */
	.cmsmasters_single_product .product_title {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h2_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h2_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h2_font_text_decoration'] . ";
		letter-spacing:0;
	}
	/* Finish H2 Font */
	
		
	/* Start H3 Font */
	ul.order_details, 
	.shop_attributes th, 
	section.products > h2, 
	.post_comments .post_comments_title, 
	.woocommerce-loop-category__title {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h3_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h3_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h3_font_text_decoration'] . ";
		letter-spacing:0;
	}
	
	.woocommerce-billing-fields > h3, 
	.woocommerce-shipping-fields > h3, 
	.cmsmasters_single_product .product_title {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] + 2) . "px;
	}
	
	.shop_attributes th {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h3_font_font_size'] - 4) . "px;
	}
	/* Finish H3 Font */
	
	
	/* Start H5 Font */
	.woocommerce-order-pay .shop_table .order-total td, 
	.woocommerce-order-pay .shop_table th.product-name, 
	.shop_table.woocommerce-checkout-review-order-table .order-total td, 
	.shop_table.woocommerce-checkout-review-order-table th.product-name, 
	.shop_table.order_details td a, 
	.shop_table.order_details th a, 
	.shop_table thead th, 
	.cmsmasters_woo_wrap_result .woocommerce-result-count, 
	ul.order_details strong, 
	.widget_layered_nav ul li, 
	.widget_layered_nav ul li a, 
	.widget_layered_nav_filters ul li, 
	.widget_layered_nav_filters ul li a, 
	.onsale, 
	.out-of-stock, 
	.stock, 
	.cmsmasters_product .cmsmasters_product_cat, 
	.cmsmasters_product .cmsmasters_product_cat a, 
	.cmsmasters_single_product .price {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h5_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h5_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_text_decoration'] . ";
		letter-spacing:0.1em;
	}
	
	.cmsmasters_single_product .price {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h5_font_font_size'] + 2) . "px;
		font-weight:bold; /* static */
	}	
	
	@media only screen and (max-width: 1440px) {	
		ul.order_details {
			font-size:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['galleria-metropolia' . '_h5_font_line_height'] . "px;
		}
	}
	/* Finish H5 Font */
	
	
	/* Start H6 Font */
	.widget > .product_list_widget .reviewer,
	.widget > .product_list_widget .amount, 
	.widget_price_filter .price_slider_amount .price_label, 
	.form-row label,
	.form-row label a,
	.widget_shopping_cart_content .total .amount, 
	.widget_shopping_cart_content .total strong, 
	.widget_shopping_cart_content .cart_list .quantity, 
	.cmsmasters_product .price, 
	.cmsmasters_product .button_to_cart, 
	.cmsmasters_single_product .product_meta > span {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_h6_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['galleria-metropolia' . '_h6_font_text_decoration'] . ";
		letter-spacing:0.1em;
	}
	
	.cmsmasters_product .price del {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_h6_font_font_size'] - 2) . "px;
		text-decoration:line-through;
	}
	
	.widget_shopping_cart_content .total strong {
		font-weight:normal; /* static */
		letter-spacing:2px; /* static */
	}
	/* Finish H6 Font */
	
	
	/* Start Button Font */
	.widget_price_filter .price_slider_amount .button,
	.widget_shopping_cart_content .buttons .button, 
	.widget_price_filter .price_slider_amount .price_label .to, 
	.widget_price_filter .price_slider_amount .price_label .from {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_button_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_button_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_button_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['galleria-metropolia' . '_button_font_text_transform'] . ";
		letter-spacing:0.1em;
	}
	
	.widget_shopping_cart_content .buttons .button {
		font-size:" . ((int) $cmsmasters_option['galleria-metropolia' . '_button_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] - 14) . "px;
	}
		
	.widget_price_filter .price_slider_amount .button, 
	.widget_price_filter .price_slider_amount .price_label {
		line-height:" . ((int) $cmsmasters_option['galleria-metropolia' . '_button_font_line_height'] - 14) . "px;
	}
	
	.widget_price_filter .price_slider_amount .price_label {
		font-weight:normal; /* static */
	}
	/* Finish Button Font */
	
	
	/* Start Text Fields Font */
	.select2-container .select2-selection--single, 
	.select2-dropdown {
		font-family:" . galleria_metropolia_get_google_font($cmsmasters_option['galleria-metropolia' . '_input_font_google_font']) . $cmsmasters_option['galleria-metropolia' . '_input_font_system_font'] . ";
		font-size:" . $cmsmasters_option['galleria-metropolia' . '_input_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['galleria-metropolia' . '_input_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['galleria-metropolia' . '_input_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['galleria-metropolia' . '_input_font_font_style'] . ";
		letter-spacing:0;
	}
	/* Finish Text Fields Font */
	
	
	/* Start Small Text Font */
	/* Finish Small Text Font */

/***************** Finish WooCommerce Font Styles ******************/

";
	
	
	return $custom_css;
}

add_filter('galleria_metropolia_theme_fonts_filter', 'galleria_metropolia_woocommerce_fonts');

