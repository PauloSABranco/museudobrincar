<?php 
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version 	1.0.6
 * 
 * Admin Panel General Options
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_options_general_tabs() {
	$cmsmasters_option = galleria_metropolia_get_global_options();
	
	$tabs = array();
	
	$tabs['general'] = esc_attr__('General', 'galleria-metropolia');
	
	if ($cmsmasters_option['galleria-metropolia' . '_theme_layout'] === 'boxed') {
		$tabs['bg'] = esc_attr__('Background', 'galleria-metropolia');
	}
	
	if (CMSMASTERS_THEME_STYLE_COMPATIBILITY) {
		$tabs['theme_style'] = esc_attr__('Theme Style', 'galleria-metropolia');
	}
	
	$tabs['header'] = esc_attr__('Header', 'galleria-metropolia');
	$tabs['content'] = esc_attr__('Content', 'galleria-metropolia');
	$tabs['footer'] = esc_attr__('Footer', 'galleria-metropolia');
	
	return apply_filters('cmsmasters_options_general_tabs_filter', $tabs);
}


function galleria_metropolia_options_general_sections() {
	$tab = galleria_metropolia_get_the_tab();
	
	switch ($tab) {
	case 'general':
		$sections = array();
		
		$sections['general_section'] = esc_attr__('General Options', 'galleria-metropolia');
		
		break;
	case 'bg':
		$sections = array();
		
		$sections['bg_section'] = esc_attr__('Background Options', 'galleria-metropolia');
		
		break;
	case 'theme_style':
		$sections = array();
		
		$sections['theme_style_section'] = esc_attr__('Theme Design Style', 'galleria-metropolia');
		
		break;
	case 'header':
		$sections = array();
		
		$sections['header_section'] = esc_attr__('Header Options', 'galleria-metropolia');
		
		break;
	case 'content':
		$sections = array();
		
		$sections['content_section'] = esc_attr__('Content Options', 'galleria-metropolia');
		
		break;
	case 'footer':
		$sections = array();
		
		$sections['footer_section'] = esc_attr__('Footer Options', 'galleria-metropolia');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return apply_filters('cmsmasters_options_general_sections_filter', $sections, $tab);
} 


function galleria_metropolia_options_general_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = galleria_metropolia_get_the_tab();
	}
	
	$options = array();
	
	
	$defaults = galleria_metropolia_settings_general_defaults();
	
	
	switch ($tab) {
	case 'general':
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_theme_layout', 
			'title' => esc_html__('Theme Layout', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_theme_layout'], 
			'choices' => array( 
				esc_html__('Liquid', 'galleria-metropolia') . '|liquid', 
				esc_html__('Boxed', 'galleria-metropolia') . '|boxed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_logo_type', 
			'title' => esc_html__('Logo Type', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_logo_type'], 
			'choices' => array( 
				esc_html__('Image', 'galleria-metropolia') . '|image', 
				esc_html__('Text', 'galleria-metropolia') . '|text' 
			) 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_logo_url', 
			'title' => esc_html__('Logo Image', 'galleria-metropolia'), 
			'desc' => esc_html__('Choose your website logo image.', 'galleria-metropolia'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_logo_url'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_logo_url_retina', 
			'title' => esc_html__('Retina Logo Image', 'galleria-metropolia'), 
			'desc' => esc_html__('Choose logo image for retina displays. Logo for Retina displays should be twice the size of the default one.', 'galleria-metropolia'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_logo_url_retina'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_logo_title', 
			'title' => esc_html__('Logo Title', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_logo_title'], 
			'class' => 'nohtml' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_logo_subtitle', 
			'title' => esc_html__('Logo Subtitle', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_logo_subtitle'], 
			'class' => 'nohtml' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_logo_custom_color', 
			'title' => esc_html__('Custom Text Colors', 'galleria-metropolia'), 
			'desc' => esc_html__('enable', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_logo_custom_color'] 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_logo_title_color', 
			'title' => esc_html__('Logo Title Color', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_logo_title_color'] 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'galleria-metropolia' . '_logo_subtitle_color', 
			'title' => esc_html__('Logo Subtitle Color', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_logo_subtitle_color'] 
		);
		
		break;
	case 'bg':
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'galleria-metropolia' . '_bg_col', 
			'title' => esc_html__('Background Color', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'color', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bg_col'] 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'galleria-metropolia' . '_bg_img_enable', 
			'title' => esc_html__('Background Image Visibility', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bg_img_enable'] 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'galleria-metropolia' . '_bg_img', 
			'title' => esc_html__('Background Image', 'galleria-metropolia'), 
			'desc' => esc_html__('Choose your custom website background image url.', 'galleria-metropolia'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bg_img'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'galleria-metropolia' . '_bg_rep', 
			'title' => esc_html__('Background Repeat', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bg_rep'], 
			'choices' => array( 
				esc_html__('No Repeat', 'galleria-metropolia') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'galleria-metropolia') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'galleria-metropolia') . '|repeat-y', 
				esc_html__('Repeat', 'galleria-metropolia') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'galleria-metropolia' . '_bg_pos', 
			'title' => esc_html__('Background Position', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bg_pos'], 
			'choices' => array( 
				esc_html__('Top Left', 'galleria-metropolia') . '|top left', 
				esc_html__('Top Center', 'galleria-metropolia') . '|top center', 
				esc_html__('Top Right', 'galleria-metropolia') . '|top right', 
				esc_html__('Center Left', 'galleria-metropolia') . '|center left', 
				esc_html__('Center Center', 'galleria-metropolia') . '|center center', 
				esc_html__('Center Right', 'galleria-metropolia') . '|center right', 
				esc_html__('Bottom Left', 'galleria-metropolia') . '|bottom left', 
				esc_html__('Bottom Center', 'galleria-metropolia') . '|bottom center', 
				esc_html__('Bottom Right', 'galleria-metropolia') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'galleria-metropolia' . '_bg_att', 
			'title' => esc_html__('Background Attachment', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bg_att'], 
			'choices' => array( 
				esc_html__('Scroll', 'galleria-metropolia') . '|scroll', 
				esc_html__('Fixed', 'galleria-metropolia') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'galleria-metropolia' . '_bg_size', 
			'title' => esc_html__('Background Size', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'galleria-metropolia') . '|auto', 
				esc_html__('Cover', 'galleria-metropolia') . '|cover', 
				esc_html__('Contain', 'galleria-metropolia') . '|contain' 
			) 
		);
		
		break;
	case 'theme_style':
		$options[] = array( 
			'section' => 'theme_style_section', 
			'id' => 'galleria-metropolia' . '_theme_style', 
			'title' => esc_html__('Choose Theme Style', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select_theme_style', 
			'std' => '', 
			'choices' => galleria_metropolia_all_theme_styles() 
		);
		
		break;
	case 'header':
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_fixed_header', 
			'title' => esc_html__('Fixed Header', 'galleria-metropolia'), 
			'desc' => esc_html__('enable', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_fixed_header'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_overlaps', 
			'title' => esc_html__('Header Overlaps Content by Default', 'galleria-metropolia'), 
			'desc' => esc_html__('enable', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_overlaps'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_top_line', 
			'title' => esc_html__('Top Line', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_top_line'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_top_height', 
			'title' => esc_html__('Top Height', 'galleria-metropolia'), 
			'desc' => esc_html__('pixels', 'galleria-metropolia'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_top_height'], 
			'min' => '10' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_top_line_short_info', 
			'title' => esc_html__('Top Short Info', 'galleria-metropolia'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'galleria-metropolia') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_top_line_short_info'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_top_line_add_cont', 
			'title' => esc_html__('Top Additional Content', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_top_line_add_cont'], 
			'choices' => array( 
				esc_html__('None', 'galleria-metropolia') . '|none', 
				esc_html__('Top Line Social Icons (will be shown if Cmsmasters Content Composer plugin is active)', 'galleria-metropolia') . '|social', 
				esc_html__('Top Line Navigation (will be shown if set in Appearance - Menus tab)', 'galleria-metropolia') . '|nav' 
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_styles', 
			'title' => esc_html__('Header Styles', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_styles'], 
			'choices' => array( 
				esc_html__('Default Style', 'galleria-metropolia') . '|default', 
				esc_html__('Compact Style Left Navigation', 'galleria-metropolia') . '|l_nav', 
				esc_html__('Compact Style Right Navigation', 'galleria-metropolia') . '|r_nav', 
				esc_html__('Compact Style Center Navigation', 'galleria-metropolia') . '|c_nav'
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_mid_height', 
			'title' => esc_html__('Header Middle Height', 'galleria-metropolia'), 
			'desc' => esc_html__('pixels', 'galleria-metropolia'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_mid_height'], 
			'min' => '40' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_bot_height', 
			'title' => esc_html__('Header Bottom Height', 'galleria-metropolia'), 
			'desc' => esc_html__('pixels', 'galleria-metropolia'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_bot_height'], 
			'min' => '20' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_search', 
			'title' => esc_html__('Header Search', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_search'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_add_cont', 
			'title' => esc_html__('Header Additional Content', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_add_cont'], 
			'choices' => array( 
				esc_html__('None', 'galleria-metropolia') . '|none', 
				esc_html__('Header Social Icons (will be shown if Cmsmasters Content Composer plugin is active)', 'galleria-metropolia') . '|social', 
				esc_html__('Header Custom HTML', 'galleria-metropolia') . '|cust_html' 
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'galleria-metropolia' . '_header_add_cont_cust_html', 
			'title' => esc_html__('Header Custom HTML', 'galleria-metropolia'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'galleria-metropolia') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_header_add_cont_cust_html'], 
			'class' => '' 
		);
		
		break;
	case 'content':
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_layout', 
			'title' => esc_html__('Layout Type by Default', 'galleria-metropolia'), 
			'desc' => esc_html__('Choosing layout with a sidebar please make sure to add widgets to the Sidebar in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'galleria-metropolia'), 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_archives_layout', 
			'title' => esc_html__('Archives Layout Type', 'galleria-metropolia'), 
			'desc' => esc_html__('Choosing layout with a sidebar please make sure to add widgets to the Archive Sidebar in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'galleria-metropolia'), 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_archives_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_search_layout', 
			'title' => esc_html__('Search Layout Type', 'galleria-metropolia'), 
			'desc' => esc_html__('Choosing layout with a sidebar please make sure to add widgets to the Search Sidebar in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'galleria-metropolia'), 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_search_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_other_layout', 
			'title' => esc_html__('Other Layout Type', 'galleria-metropolia'), 
			'desc' => esc_html__('Layout for pages of non-listed types. Choosing layout with a sidebar please make sure to add widgets to the Sidebar in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'galleria-metropolia'), 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_other_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'galleria-metropolia') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_alignment', 
			'title' => esc_html__('Heading Alignment by Default', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_alignment'], 
			'choices' => array( 
				esc_html__('Left', 'galleria-metropolia') . '|left', 
				esc_html__('Right', 'galleria-metropolia') . '|right', 
				esc_html__('Center', 'galleria-metropolia') . '|center' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_scheme', 
			'title' => esc_html__('Heading Custom Color Scheme by Default', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_bg_image_enable', 
			'title' => esc_html__('Heading Background Image Visibility by Default', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_bg_image_enable'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_bg_image', 
			'title' => esc_html__('Heading Background Image by Default', 'galleria-metropolia'), 
			'desc' => esc_html__('Choose your custom heading background image by default.', 'galleria-metropolia'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_bg_image'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_bg_repeat', 
			'title' => esc_html__('Heading Background Repeat by Default', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_bg_repeat'], 
			'choices' => array( 
				esc_html__('No Repeat', 'galleria-metropolia') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'galleria-metropolia') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'galleria-metropolia') . '|repeat-y', 
				esc_html__('Repeat', 'galleria-metropolia') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_bg_attachment', 
			'title' => esc_html__('Heading Background Attachment by Default', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_bg_attachment'], 
			'choices' => array( 
				esc_html__('Scroll', 'galleria-metropolia') . '|scroll', 
				esc_html__('Fixed', 'galleria-metropolia') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_bg_size', 
			'title' => esc_html__('Heading Background Size by Default', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'galleria-metropolia') . '|auto', 
				esc_html__('Cover', 'galleria-metropolia') . '|cover', 
				esc_html__('Contain', 'galleria-metropolia') . '|contain' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_bg_color', 
			'title' => esc_html__('Heading Background Color Overlay by Default', 'galleria-metropolia'), 
			'desc' => '',  
			'type' => 'rgba', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_bg_color'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_heading_height', 
			'title' => esc_html__('Heading Height by Default', 'galleria-metropolia'), 
			'desc' => esc_html__('pixels', 'galleria-metropolia'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_heading_height'], 
			'min' => '0' 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_breadcrumbs', 
			'title' => esc_html__('Breadcrumbs Visibility by Default', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_breadcrumbs'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_bottom_scheme', 
			'title' => esc_html__('Bottom Custom Color Scheme', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bottom_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_bottom_sidebar', 
			'title' => esc_html__('Bottom Sidebar Visibility by Default', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia') . '<br><br>' . esc_html__('Please make sure to add widgets in the Appearance - Widgets tab. The empty sidebar won\'t be displayed.', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bottom_sidebar'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'galleria-metropolia' . '_bottom_sidebar_layout', 
			'title' => esc_html__('Bottom Sidebar Layout by Default', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_bottom_sidebar_layout'], 
			'choices' => array( 
				'1/1|11', 
				'1/2 + 1/2|1212', 
				'1/3 + 2/3|1323', 
				'2/3 + 1/3|2313', 
				'1/4 + 3/4|1434', 
				'3/4 + 1/4|3414', 
				'1/3 + 1/3 + 1/3|131313', 
				'1/2 + 1/4 + 1/4|121414', 
				'1/4 + 1/2 + 1/4|141214', 
				'1/4 + 1/4 + 1/2|141412', 
				'1/4 + 1/4 + 1/4 + 1/4|14141414' 
			) 
		);
		
		break;
	case 'footer':
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_scheme', 
			'title' => esc_html__('Footer Custom Color Scheme', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_type', 
			'title' => esc_html__('Footer Type', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_type'], 
			'choices' => array( 
				esc_html__('Default', 'galleria-metropolia') . '|default', 
				esc_html__('Small', 'galleria-metropolia') . '|small' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_additional_content', 
			'title' => esc_html__('Footer Additional Content', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_additional_content'], 
			'choices' => array( 
				esc_html__('None', 'galleria-metropolia') . '|none', 
				esc_html__('Footer Navigation (will be shown if set in Appearance - Menus tab)', 'galleria-metropolia') . '|nav', 
				esc_html__('Social Icons (will be shown if Cmsmasters Content Composer plugin is active)', 'galleria-metropolia') . '|social', 
				esc_html__('Custom HTML', 'galleria-metropolia') . '|text' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_logo', 
			'title' => esc_html__('Footer Logo', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_logo'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_logo_url', 
			'title' => esc_html__('Footer Logo', 'galleria-metropolia'), 
			'desc' => esc_html__('Choose your website footer logo image.', 'galleria-metropolia'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_logo_url'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_logo_url_retina', 
			'title' => esc_html__('Footer Logo for Retina', 'galleria-metropolia'), 
			'desc' => esc_html__('Choose your website footer logo image for retina.', 'galleria-metropolia'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_logo_url_retina'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_nav', 
			'title' => esc_html__('Footer Navigation', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_nav'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_social', 
			'title' => esc_html__('Footer Social Icons (will be shown if Cmsmasters Content Composer plugin is active)', 'galleria-metropolia'), 
			'desc' => esc_html__('show', 'galleria-metropolia'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_social'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_html', 
			'title' => esc_html__('Footer Custom HTML', 'galleria-metropolia'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'galleria-metropolia') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_html'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'galleria-metropolia' . '_footer_copyright', 
			'title' => esc_html__('Copyright Text', 'galleria-metropolia'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['galleria-metropolia' . '_footer_copyright'], 
			'class' => '' 
		);
		
		break;
	}
	
	return apply_filters('cmsmasters_options_general_fields_filter', $options, $tab);
}

