<?php 
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version		1.0.0
 * 
 * Admin Panel Theme Settings Import/Export
 * Created by CMSMasters
 * 
 */


function galleria_metropolia_options_demo_tabs() {
	$tabs = array();
	
	
	$tabs['import'] = esc_attr__('Import', 'galleria-metropolia');
	$tabs['export'] = esc_attr__('Export', 'galleria-metropolia');
	
	
	return $tabs;
}


function galleria_metropolia_options_demo_sections() {
	$tab = galleria_metropolia_get_the_tab();
	
	
	switch ($tab) {
	case 'import':
		$sections = array();
		
		$sections['import_section'] = esc_html__('Theme Settings Import', 'galleria-metropolia');
		
		
		break;
	case 'export':
		$sections = array();
		
		$sections['export_section'] = esc_html__('Theme Settings Export', 'galleria-metropolia');
		
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	
	return $sections;
} 


function galleria_metropolia_options_demo_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = galleria_metropolia_get_the_tab();
	}
	
	
	$options = array();
	
	
	switch ($tab) {
	case 'import':
		$options[] = array( 
			'section' => 'import_section', 
			'id' => 'galleria-metropolia' . '_demo_import', 
			'title' => esc_html__('Theme Settings', 'galleria-metropolia'), 
			'desc' => esc_html__("Enter your theme settings data here and click 'Import' button", 'galleria-metropolia') . (CMSMASTERS_THEME_STYLE_COMPATIBILITY ? '<span class="descr_note">' . esc_html__("Please note that when importing theme settings, these settings will be applied to the appropriate Theme Style (with the same name).", 'galleria-metropolia') . '<br />' . esc_html__("To see these settings applied, please enable appropriate", 'galleria-metropolia') . ' <a href="' . esc_url(admin_url('admin.php?page=cmsmasters-settings&tab=theme_style')) . '">' . esc_html__("Theme Style", 'galleria-metropolia') . '</a>.</span>' : ''), 
			'type' => 'textarea', 
			'std' => '', 
			'class' => '' 
		);
		
		
		break;
	case 'export':
		$options[] = array( 
			'section' => 'export_section', 
			'id' => 'galleria-metropolia' . '_demo_export', 
			'title' => esc_html__('Theme Settings', 'galleria-metropolia'), 
			'desc' => esc_html__("Click here to export your theme settings data to the file.", 'galleria-metropolia') . (CMSMASTERS_THEME_STYLE_COMPATIBILITY ? '<span class="descr_note">' . esc_html__("Please note, that when exporting theme settings, you will export settings for the currently active Theme Style.", 'galleria-metropolia') . '<br />' . esc_html__("Theme Style can be set", 'galleria-metropolia') . ' <a href="' . esc_url(admin_url('admin.php?page=cmsmasters-settings&tab=theme_style')) . '">' . esc_html__("here", 'galleria-metropolia') . '</a>.</span>' : ''), 
			'type' => 'button', 
			'std' => esc_html__('Export Theme Settings', 'galleria-metropolia'), 
			'class' => 'cmsmasters-demo-export' 
		);
		
		
		break;
	}
	
	
	return $options;	
}

