<?php
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version		1.0.0
 * 
 * Posts Slider Post Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_metadata = explode(',', $cmsmasters_post_metadata);


$title = in_array('title', $cmsmasters_metadata) ? true : false;
$excerpt = (in_array('excerpt', $cmsmasters_metadata) && galleria_metropolia_slider_post_check_exc_cont('post')) ? true : false;
$date = in_array('date', $cmsmasters_metadata) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsmasters_metadata))) ? true : false;
$author = in_array('author', $cmsmasters_metadata) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsmasters_metadata))) ? true : false;
$likes = in_array('likes', $cmsmasters_metadata) ? true : false;
$more = in_array('more', $cmsmasters_metadata) ? true : false;


$cmsmasters_post_format = get_post_format();

?>
<!-- Start Posts Slider Post Article -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_slider_post'); ?>>
	<div class="cmsmasters_slider_post_outer">
	<?php
		echo '<div class="cmsmasters_slider_post_img_wrap">';
		
			galleria_metropolia_thumb_rollover(get_the_ID(), 'cmsmasters-square-thumb', false, false, false, false, false, false, false, false, true, false, false);
		
		echo '</div>';
		
		
		if ($categories) {
			echo '<div class="cmsmasters_slider_post_cont_info first entry-meta">';
				
				$categories ? galleria_metropolia_get_slider_post_category(get_the_ID(), 'category', 'post') : '';
				
			echo '</div>';
		}
		
		
		$title ? galleria_metropolia_slider_post_heading(get_the_ID(), 'post', 'h4') : '';
		
		
		if ($date || $author) {
			echo '<div class="cmsmasters_slider_post_cont_info second entry-meta">';
				
				$date ? galleria_metropolia_get_slider_post_date('post') : '';
				
				$author ? galleria_metropolia_get_slider_post_author('post') : '';
				
			echo '</div>';
		}
		
		
		$excerpt ? galleria_metropolia_slider_post_exc_cont('post') : '';
		
		
		if ($likes || $comments) {
			echo '<div class="cmsmasters_slider_post_cont_info third entry-meta">';
				
				$likes ? galleria_metropolia_slider_post_like('post') : '';
				
				$comments ? galleria_metropolia_get_slider_post_comments('post') : '';
				
			echo '</div>';
		}
		
		
		if ($more) {
			echo '<footer class="cmsmasters_slider_post_footer entry-meta">';
				
				$more ? galleria_metropolia_slider_post_more(get_the_ID()) : '';
				
			echo '</footer>';
		}
	?>
	</div>
</article>
<!-- Finish Posts Slider Post Article -->

