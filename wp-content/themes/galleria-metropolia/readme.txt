/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version 	1.0.6
 * 
 * Theme Information
 * Created by CMSMasters
 * 
 */

To update your theme please use the files list from the FILE LOGS below and substitute/add the listed files on your server with the same files in the Updates folder.

Important: after you have updated the theme, in your admin panel please proceed to
Theme Settings - Fonts and click "Save" in any tab,
then proceed to 
Theme Settings - Colors and click "Save" in any tab here.


--------------------------------------
Version 1.0.7: files operations:
	Theme Files edited:
		functions.php
		gutenberg/cmsmasters-framework/theme-style/css/editor-style.css
		gutenberg/cmsmasters-framework/theme-style/css/frontend-style.css
		gutenberg/cmsmasters-framework/theme-style/css/less/editor-style.less
		gutenberg/cmsmasters-framework/theme-style/function/module-fonts.php
		style.css
		theme-framework/theme-style/css/less/general.less
		theme-framework/theme-style/css/less/style.less
		theme-framework/theme-style/css/style.css
		theme-vars/languages/galleria-metropolia.pot
		theme-vars/plugin-activator.php
		theme-vars/theme-style/admin/demo-content-importer.php
		theme-vars/theme-style/css/vars-style.css


	Theme Files Update
		Proceed to wp-content\plugins\cmsmasters-contact-form-builder
		and update all files in this folder to version 1.4.4

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 2.3.6
		
--------------------------------------


Version 1.0.6: files operations:
	Theme Files edited:
		framework\admin\inc\config-functions.php
		framework\admin\options\cmsmasters-theme-options-other.php
		framework\admin\options\cmsmasters-theme-options-page.php
		framework\admin\options\cmsmasters-theme-options-post.php
		framework\admin\settings\cmsmasters-theme-settings-font.php
		framework\admin\settings\cmsmasters-theme-settings-general.php
		framework\admin\settings\cmsmasters-theme-settings.php
		framework\admin\settings\css\cmsmasters-theme-settings.css
		framework\admin\settings\inc\cmsmasters-helper-functions.php
		framework\admin\settings\js\cmsmasters-theme-settings.js
		framework\class\browser.php
		framework\function\general-functions.php
		functions.php
		js\jquery.script.js
		js\smooth-sticky.min.js
		search.php
		style.css
		theme-framework\theme-style\class\theme-widgets.php
		theme-framework\theme-style\css\less\general.less
		theme-framework\theme-style\css\less\style.less
		theme-framework\theme-style\css\style.css
		theme-framework\theme-style\function\template-functions.php
		theme-framework\theme-style\function\theme-fonts.php
		theme-vars\plugin-activator.php
		theme-vars\plugins\cmsmasters-contact-form-builder.zip
		theme-vars\plugins\cmsmasters-content-composer.zip
		theme-vars\plugins\cmsmasters-importer.zip
		theme-vars\plugins\cmsmasters-mega-menu.zip
		theme-vars\theme-style\admin\demo-content-importer.php
		theme-vars\theme-style\admin\theme-settings-defaults.php
		tribe-events\cmsmasters-framework\theme-style\admin\plugin-settings.php
		tribe-events\cmsmasters-framework\theme-style\cmsmasters-plugin-functions.php
		tribe-events\cmsmasters-framework\theme-style\css\less\plugin-style.less
		tribe-events\cmsmasters-framework\theme-style\css\plugin-style.css
		tribe-events\cmsmasters-framework\theme-style\function\plugin-colors.php
		woocommerce\cmsmasters-framework\theme-style\templates\content-single-product.php
		woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-image.php
		woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-thumbnails.php
		woocommerce\content-widget-product.php
		woocommerce\single-product\product-image.php
		woocommerce\single-product\product-thumbnails.php
		framework\admin\options\cmsmasters-theme-options.php
		theme-vars\languages\galleria-metropolia.pot
		theme-vars\plugins\revslider.zip
		theme-vars\theme-style\admin\demo-content\alternative\theme-settings.txt
		theme-vars\theme-style\admin\demo-content\main\theme-settings.txt
		tribe-events\cmsmasters-framework\theme-style\templates\pro\widgets\modules\single-event.php
		woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		woocommerce\cmsmasters-framework\theme-style\function\plugin-colors.php

	Theme Files added:
		gutenberg\cmsmasters-framework\theme-style\cmsmasters-module-functions.php
		gutenberg\cmsmasters-framework\theme-style\css\editor-style.css
		gutenberg\cmsmasters-framework\theme-style\css\frontend-style.css
		gutenberg\cmsmasters-framework\theme-style\css\less\editor-style.less
		gutenberg\cmsmasters-framework\theme-style\css\less\module-style.less
		gutenberg\cmsmasters-framework\theme-style\css\module-rtl.css
		gutenberg\cmsmasters-framework\theme-style\function\module-colors.php
		gutenberg\cmsmasters-framework\theme-style\function\module-fonts.php
		gutenberg\cmsmasters-framework\theme-style\js\editor-options.js
		theme-vars\plugins\cmsmasters-custom-fonts.zip

		Proceed to wp-content\plugins\cmsmasters-contact-form-builder
		and update all files in this folder to version 1.4.4

		Proceed to wp-content\plugins\cmsmasters-custom-fonts
		and update all files in this folder to version 1.0.1
		
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.4.8.1

		
--------------------------------------

Version 1.0.5: files operations:
	Theme Files edited:
		framework\admin\inc\css\admin-theme-styles.css
		framework\admin\options\cmsmasters-theme-options-post.php
		framework\admin\options\cmsmasters-theme-options.php
		framework\admin\options\css\cmsmasters-theme-options.css
		framework\admin\options\js\cmsmasters-theme-options.js
		framework\admin\settings\cmsmasters-theme-settings.php
		framework\admin\settings\css\cmsmasters-theme-settings-rtl.css
		framework\admin\settings\css\cmsmasters-theme-settings.css
		framework\admin\settings\inc\cmsmasters-helper-functions.php
		framework\class\browser.php
		framework\class\class-tgm-plugin-activation.php
		framework\function\breadcrumbs.php
		framework\function\general-functions.php
		framework\function\likes.php
		framework\function\theme-categories-icon.php
		framework\function\views.php
		js\jquery.script.js
		js\smooth-sticky.min.js
		theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-posts-slider.php
		theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-pricing-table-item.php
		theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-stat.php
		theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-toggles.php
		theme-framework\theme-style\css\less\general.less
		theme-framework\theme-style\css\less\style.less
		theme-framework\theme-style\css\style.css
		theme-framework\theme-style\function\template-functions-post.php
		theme-framework\theme-style\function\template-functions-profile.php
		theme-framework\theme-style\function\template-functions-project.php
		theme-framework\theme-style\function\template-functions-shortcodes.php
		theme-framework\theme-style\function\template-functions.php
		theme-framework\theme-style\js\jquery.theme-script.js
		theme-framework\theme-style\theme-functions.php
		theme-vars\plugin-activator.php
		tribe-events\cmsmasters-framework\theme-style\css\less\plugin-style.less
		tribe-events\cmsmasters-framework\theme-style\css\plugin-style.css
		tribe-events\cmsmasters-framework\theme-style\templates\day\single-event.php
		tribe-events\cmsmasters-framework\theme-style\templates\list\single-event.php
		tribe-events\cmsmasters-framework\theme-style\templates\modules\bar.php
		tribe-events\cmsmasters-framework\theme-style\templates\pro\widgets\modules\single-event.php
		woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		woocommerce\cmsmasters-framework\theme-style\function\plugin-template-functions.php
		woocommerce\cmsmasters-framework\theme-style\js\jquery.plugin-script.js
		woocommerce\cmsmasters-framework\theme-style\templates\content-product.php
		woocommerce\cmsmasters-framework\theme-style\templates\content-single-product.php
		woocommerce\cmsmasters-framework\theme-style\templates\content-widget-product.php
		woocommerce\cmsmasters-framework\theme-style\templates\single-product\meta.php
		functions.php
		theme-framework\theme-style\function\theme-colors-primary.php
		theme-vars\languages\galleria-metropolia.pot
		theme-vars\plugins\cmsmasters-contact-form-builder.zip
		theme-vars\plugins\cmsmasters-content-composer.zip
		theme-vars\theme-style\admin\theme-settings-defaults.php
		theme-vars\theme-style\css\styles\galleria-metropolia.css
		tribe-events\cmsmasters-framework\theme-style\templates\widgets\list-widget.php
		woocommerce\cmsmasters-framework\theme-style\css\less\plugin-adaptive.less
		woocommerce\cmsmasters-framework\theme-style\css\plugin-adaptive.css
		woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		woocommerce\cmsmasters-framework\theme-style\function\plugin-colors.php

	Theme Files added:
		theme-vars\plugins\cmsmasters-importer.zip
		theme-vars\theme-style\admin\demo-content-importer.php
		theme-vars\theme-style\admin\demo-content\alternative\content.xml
		theme-vars\theme-style\admin\demo-content\alternative\sliders\layerslider\LayerSlider.zip
		theme-vars\theme-style\admin\demo-content\alternative\sliders\revslider\home_slider.zip
		theme-vars\theme-style\admin\demo-content\alternative\sliders\revslider\shortcode_slider.zip
		theme-vars\theme-style\admin\demo-content\alternative\theme-settings.txt
		theme-vars\theme-style\admin\demo-content\alternative\widgets.json
		theme-vars\theme-style\admin\demo-content\main\content.xml
		theme-vars\theme-style\admin\demo-content\main\sliders\layerslider\LayerSlider.zip
		theme-vars\theme-style\admin\demo-content\main\sliders\revslider\home2_slider.zip
		theme-vars\theme-style\admin\demo-content\main\sliders\revslider\shortcode_slider.zip
		theme-vars\theme-style\admin\demo-content\main\theme-settings.txt
		theme-vars\theme-style\admin\demo-content\main\widgets.json

	Theme Files deleted:
		theme-vars\plugins\envato-market.zip
		js\query-loader.min.js
		
--------------------------------------

Version 1.0.4: files operations:
	Theme Files edited:
		framework\admin\options\cmsmasters-theme-options.php
		framework\admin\settings\cmsmasters-theme-settings.php
		framework\function\general-functions.php
		functions.php
		page.php
		sitemap.php
		style.css
		theme-framework\theme-style\css\adaptive.css
		theme-framework\theme-style\css\less\adaptive.less
		theme-framework\theme-style\css\less\style.less
		theme-framework\theme-style\css\style.css
		theme-framework\theme-style\function\theme-colors-primary.php
		theme-framework\theme-style\function\theme-fonts.php
		theme-framework\theme-style\js\jquery.isotope.mode.js
		theme-framework\theme-style\postType\blog\post-single.php
		theme-framework\theme-style\postType\portfolio\project-single.php
		theme-framework\theme-style\postType\profile\profile-single.php
		theme-framework\theme-style\postType\quote\quote-slider.php
		theme-vars\languages\galleria-metropolia.pot
		theme-vars\plugin-activator.php
		theme-vars\plugins\cmsmasters-content-composer.zip
		theme-vars\plugins\revslider.zip
		theme-vars\theme-style\admin\fonts\config-custom.json
		theme-vars\theme-style\admin\theme-settings-defaults.php
		theme-vars\theme-style\css\fontello-custom.css
		theme-vars\theme-style\css\fonts\fontello-custom.eot
		theme-vars\theme-style\css\fonts\fontello-custom.svg
		theme-vars\theme-style\css\fonts\fontello-custom.ttf
		theme-vars\theme-style\css\fonts\fontello-custom.woff
		tribe-events\cmsmasters-framework\theme-style\cmsmasters-plugin-functions.php
		tribe-events\cmsmasters-framework\theme-style\css\less\plugin-style.less
		tribe-events\cmsmasters-framework\theme-style\css\plugin-style.css
		tribe-events\cmsmasters-framework\theme-style\templates\month\tooltip.php
		tribe-events\cmsmasters-framework\theme-style\templates\pro\week\tooltip.php
		woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		woocommerce\content-single-product.php

		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 2.2.8
		
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.4.8
		
--------------------------------------

Version 1.0.3: files operations:
	comments.php
	framework\admin\settings\cmsmasters-theme-settings.php
	framework\class\browser.php
	framework\function\breadcrumbs.php
	framework\function\general-functions.php
	framework\function\likes.php
	framework\function\views.php
	functions.php
	js\jquery.script.js
	js\smooth-sticky.min.js
	readme.txt
	theme-framework\theme-style\class\theme-widgets.php
	theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-posts-slider.php
	theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-pricing-table-item.php
	theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-stat.php
	theme-framework\theme-style\cmsmasters-c-c\shortcodes\cmsmasters-toggles.php
	theme-framework\theme-style\css\less\style.less
	theme-framework\theme-style\css\style.css
	theme-framework\theme-style\function\template-functions-post.php
	theme-framework\theme-style\function\template-functions-profile.php
	theme-framework\theme-style\function\template-functions-project.php
	theme-framework\theme-style\function\template-functions-shortcodes.php
	theme-framework\theme-style\function\template-functions.php
	theme-framework\theme-style\template\footer.php
	theme-vars\languages\galleria-metropolia.pot
	theme-vars\plugin-activator.php
	theme-vars\plugins\cmsmasters-content-composer.zip
	theme-vars\plugins\LayerSlider.zip
	theme-vars\plugins\revslider.zip
	theme-vars\theme-style\admin\theme-settings-defaults.php
	tribe-events\cmsmasters-framework\theme-style\templates\modules\bar.php
	tribe-events\cmsmasters-framework\theme-style\templates\month\single-event.php
	tribe-events\cmsmasters-framework\theme-style\templates\pro\modules\meta\additional-fields.php
	tribe-events\cmsmasters-framework\theme-style\templates\pro\week\single-event.php
	tribe-events\cmsmasters-framework\theme-style\templates\pro\widgets\modules\single-event.php
	woocommerce\archive-product.php
	woocommerce\cmsmasters-framework\theme-style\function\plugin-template-functions.php
	woocommerce\cmsmasters-framework\theme-style\templates\archive-product.php
	woocommerce\cmsmasters-framework\theme-style\templates\content-product.php
	woocommerce\cmsmasters-framework\theme-style\templates\content-single-product.php
	woocommerce\cmsmasters-framework\theme-style\templates\content-widget-product.php
	woocommerce\content-product.php
	woocommerce\single-product.php
		
		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 2.2.4
		
		Proceed to wp-content\plugins\LayerSlider
		and update all files in this folder to version 6.7.6
		
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.4.7.4

--------------------------------------

Version 1.0.2: files operations:
		
Theme Files edited:
	galleria-metropolia\theme-framework\theme-style\css\styles\galleria-metropolia.css
	galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
	galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
	galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\function\plugin-colors.php
	galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\function\plugin-fonts.php
	galleria-metropolia\readme.txt
	galleria-metropolia\style.css
	galleria-metropolia\theme-framework\languages\galleria-metropolia.pot
	galleria-metropolia\theme-framework\plugin-activator.php
	galleria-metropolia\theme-framework\plugins\LayerSlider.zip
	galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\templates\single-product-reviews.php
	galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-image.php
	galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-thumbnails.php
	galleria-metropolia\woocommerce\product-searchform.php
	galleria-metropolia\woocommerce\single-product\meta.php
	galleria-metropolia\woocommerce\single-product\product-image.php
	galleria-metropolia\woocommerce\single-product\product-thumbnails.php
	galleria-metropolia\woocommerce\single-product\review.php

		Proceed to wp-content\plugins\cmsmasters-contact-form-builder
		and update all files in this folder to version 1.4.2
		
		Proceed to wp-content\plugins\cmsmasters-content-composer
		and update all files in this folder to version 2.2.0
		
		Proceed to wp-content\plugins\revslider
		and update all files in this folder to version 5.4.7.3
		
		Proceed to wp-content\plugins\envato-market
		and update all files in this folder to version 2.0.0


--------------------------------------
Version 1.0.1: files operations:
		
  Theme Files edited:
		
		galleria-metropolia\theme-framework\theme-style\css\styles\galleria-metropolia.css
		galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\function\plugin-colors.php
		galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\function\plugin-fonts.php
		galleria-metropolia\readme.txt
		galleria-metropolia\style.css
		galleria-metropolia\theme-framework\languages\galleria-metropolia.pot
		galleria-metropolia\theme-framework\plugin-activator.php
		galleria-metropolia\theme-framework\plugins\LayerSlider.zip
		galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\templates\single-product-reviews.php
		galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-image.php
		galleria-metropolia\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-thumbnails.php
		galleria-metropolia\woocommerce\product-searchform.php
		galleria-metropolia\woocommerce\single-product\meta.php
		galleria-metropolia\woocommerce\single-product\product-image.php
		galleria-metropolia\woocommerce\single-product\product-thumbnails.php
		galleria-metropolia\woocommerce\single-product\review.php


--------------------------------------
Version 1.0: Release!

