<?php
/**
 * @cmsmasters_package 	Galleria Metropolia
 * @cmsmasters_version 	1.0.0
 */


$venue_details = tribe_get_venue_details();

$events_label_plural = tribe_get_event_label_plural();

$posts = tribe_get_list_widget_events();

// Check if any event posts are found.
if ( $posts ) : ?>

	<ol class="hfeed vcalendar">
		<?php
		// Setup the post data for each event.
		foreach ( $posts as $post ) :
			setup_postdata( $post );
			$post_url = tribe_get_event_link();
			
			?>
			<li class="tribe-events-list-widget-events <?php tribe_events_event_classes() ?>">
				<div class="cmsmasters_event_big_date">
					<div class="cmsmasters_event_big_day"><?php echo tribe_get_start_date(null, false, 'd'); ?></div>
					<div class="cmsmasters_event_big_date_ovh">
						<div class="cmsmasters_event_big_month"><?php echo tribe_get_start_date(null, false, 'F'); ?></div>
						<div class="cmsmasters_event_big_week"><?php echo tribe_get_start_date(null, false, 'l'); ?></div>
					</div>
				</div>
				<?php if (get_post_thumbnail_id(get_the_ID()) != '') { ?>
				<div class="tribe-events-event-image">
					<?php galleria_metropolia_thumb(get_the_ID(), 'cmsmasters-small-thumb', false, true, false, false, false, true, false); ?>
				</div>
				<?php } ?>
				<div class="tribe-events-list-widget-content-wrap">
					<?php do_action( 'tribe_events_list_widget_before_the_event_title' ); ?>
					<!-- Event Title -->
					<h5 class="entry-title summary">
						<a href="<?php echo esc_url( $post_url ); ?>" rel="bookmark"><?php the_title(); ?></a>
					</h5>
					
					<?php do_action( 'tribe_events_list_widget_after_the_event_title' ); ?>
					<!-- Event Time -->
					
					<?php do_action( 'tribe_events_list_widget_before_the_meta' ) ?>
					
					<div class="tribe-events-event-meta vcard">
						<div class="author <?php echo esc_attr( $has_venue_address ); ?>">
							
							<!-- Schedule & Recurrence Details -->
							<div class="updated published time-details">
								<?php echo tribe_events_event_schedule_details() ?>
							</div>

							<?php if ( $venue_details ) : ?>
								<!-- Venue Display Info -->
								<div class="tribe-events-venue-details">
									<?php echo implode( ', ', $venue_details ); ?>
								</div> <!-- .tribe-events-venue-details -->
							<?php endif; ?>
							
							
							<div class="tribe-events-list-event-description tribe-events-content description entry-summary">
								<?php echo tribe_events_get_the_excerpt(); ?>
							</div>
						</div>
					</div><!-- .tribe-events-event-meta -->

					<?php do_action( 'tribe_events_list_widget_after_the_meta' ) ?>
				</div>
				<div class="cmsmasters_events_more">
					<a href="<?php echo esc_url( $post_url ); ?>" class="tribe-events-read-more button" rel="bookmark"><?php esc_html_e( 'View event', 'galleria-metropolia' ) ?></a>
				</div>
			</li>
		<?php
		endforeach;
		?>
	</ol><!-- .hfeed -->

	<p class="tribe-events-widget-link">
		<a href="<?php echo esc_url( tribe_get_events_link() ); ?>" rel="bookmark"><?php printf( esc_html__( 'View All %s', 'galleria-metropolia' ), $events_label_plural ); ?></a>
	</p>

<?php
// No events were found.
else : ?>
	<p><?php printf( esc_html__( 'There are no upcoming %s at this time.', 'galleria-metropolia' ), strtolower( $events_label_plural ) ); ?></p>
<?php
endif;
