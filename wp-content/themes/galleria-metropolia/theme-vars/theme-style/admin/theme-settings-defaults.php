<?php 
/**
 * @package 	WordPress
 * @subpackage 	Galleria Metropolia
 * @version		1.0.6
 * 
 * Theme Settings Defaults
 * Created by CMSMasters
 * 
 */


/* Theme Settings General Default Values */
if (!function_exists('galleria_metropolia_settings_general_defaults')) {

function galleria_metropolia_settings_general_defaults($id = false) {
	$settings = array( 
		'general' => array( 
			'galleria-metropolia' . '_theme_layout' => 		'liquid', 
			'galleria-metropolia' . '_logo_type' => 		'image', 
			'galleria-metropolia' . '_logo_url' => 			'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/logo.png', 
			'galleria-metropolia' . '_logo_url_retina' => 	'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/logo_retina.png', 
			'galleria-metropolia' . '_logo_title' => 		get_bloginfo('name') ? get_bloginfo('name') : 'Galleria Metropolia', 
			'galleria-metropolia' . '_logo_subtitle' => 	'', 
			'galleria-metropolia' . '_logo_custom_color' => 0, 
			'galleria-metropolia' . '_logo_title_color' => 	'', 
			'galleria-metropolia' . '_logo_subtitle_color' => '' 
		), 
		'bg' => array( 
			'galleria-metropolia' . '_bg_col' => 			'#fafafa', 
			'galleria-metropolia' . '_bg_img_enable' => 	0, 
			'galleria-metropolia' . '_bg_img' => 			'', 
			'galleria-metropolia' . '_bg_rep' => 			'no-repeat', 
			'galleria-metropolia' . '_bg_pos' => 			'top center', 
			'galleria-metropolia' . '_bg_att' => 			'scroll', 
			'galleria-metropolia' . '_bg_size' => 			'cover' 
		), 
		'header' => array( 
			'galleria-metropolia' . '_fixed_header' => 				1, 
			'galleria-metropolia' . '_header_overlaps' => 			1, 
			'galleria-metropolia' . '_header_top_line' => 			0, 
			'galleria-metropolia' . '_header_top_height' => 		'38', 
			'galleria-metropolia' . '_header_top_line_short_info' => '', 
			'galleria-metropolia' . '_header_top_line_add_cont' => 	'social', 
			'galleria-metropolia' . '_header_styles' => 			'default', 
			'galleria-metropolia' . '_header_mid_height' => 		'98', 
			'galleria-metropolia' . '_header_bot_height' => 		'60', 
			'galleria-metropolia' . '_header_search' => 			0, 
			'galleria-metropolia' . '_header_add_cont' => 			'social', 
			'galleria-metropolia' . '_header_add_cont_cust_html' => '' 
		), 
		'content' => array( 
			'galleria-metropolia' . '_layout' => 					'r_sidebar', 
			'galleria-metropolia' . '_archives_layout' => 			'r_sidebar', 
			'galleria-metropolia' . '_search_layout' => 			'r_sidebar', 
			'galleria-metropolia' . '_other_layout' => 				'r_sidebar', 
			'galleria-metropolia' . '_heading_alignment' => 		'center', 
			'galleria-metropolia' . '_heading_scheme' => 			'default', 
			'galleria-metropolia' . '_heading_bg_image_enable' => 	0, 
			'galleria-metropolia' . '_heading_bg_image' => 			'', 
			'galleria-metropolia' . '_heading_bg_repeat' => 		'no-repeat', 
			'galleria-metropolia' . '_heading_bg_attachment' => 	'scroll', 
			'galleria-metropolia' . '_heading_bg_size' => 			'cover', 
			'galleria-metropolia' . '_heading_bg_color' => 			'', 
			'galleria-metropolia' . '_heading_height' => 			'312', 
			'galleria-metropolia' . '_breadcrumbs' => 				1, 
			'galleria-metropolia' . '_bottom_scheme' => 			'first', 
			'galleria-metropolia' . '_bottom_sidebar' => 			0, 
			'galleria-metropolia' . '_bottom_sidebar_layout' => 	'14141414' 
		), 
		'footer' => array( 
			'galleria-metropolia' . '_footer_scheme' => 			'footer', 
			'galleria-metropolia' . '_footer_type' => 				'default', 
			'galleria-metropolia' . '_footer_additional_content' => 'nav', 
			'galleria-metropolia' . '_footer_logo' => 				0, 
			'galleria-metropolia' . '_footer_logo_url' => 			'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/logo_footer.png', 
			'galleria-metropolia' . '_footer_logo_url_retina' => 	'|' . get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_THEME_STYLE . '/img/logo_footer_retina.png', 
			'galleria-metropolia' . '_footer_nav' => 				0, 
			'galleria-metropolia' . '_footer_social' => 			0, 
			'galleria-metropolia' . '_footer_html' => 				'', 
			'galleria-metropolia' . '_footer_copyright' => 			'Galleria Metropolia' . ' &copy; ' . date('Y') . ' / ' . esc_html__('All Rights Reserved', 'galleria-metropolia') 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



/* Theme Settings Fonts Default Values */
if (!function_exists('galleria_metropolia_settings_font_defaults')) {

function galleria_metropolia_settings_font_defaults($id = false) {
	$settings = array( 
		'content' => array( 
			'galleria-metropolia' . '_content_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Muli', 
				'font_size' => 			'16', 
				'line_height' => 		'28', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal' 
			) 
		), 
		'link' => array( 
			'galleria-metropolia' . '_link_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Muli', 
				'font_size' => 			'16', 
				'line_height' => 		'26', 
				'font_weight' => 		'600', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'galleria-metropolia' . '_link_hover_decoration' => 	'none' 
		), 
		'nav' => array( 
			'galleria-metropolia' . '_nav_title_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Karla:400,400i,700,700i', 
				'font_size' => 			'13', 
				'line_height' => 		'26', 
				'font_weight' => 		'bold', 
				'font_style' => 		'normal', 
				'text_transform' => 	'uppercase' 
			), 
			'galleria-metropolia' . '_nav_dropdown_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Muli', 
				'font_size' => 			'14', 
				'line_height' => 		'20', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none' 
			) 
		), 
		'heading' => array( 
			'galleria-metropolia' . '_h1_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Prata', 
				'font_size' => 			'52', 
				'line_height' => 		'62', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'galleria-metropolia' . '_h2_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Prata', 
				'font_size' => 			'32', 
				'line_height' => 		'44', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'galleria-metropolia' . '_h3_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Prata', 
				'font_size' => 			'20', 
				'line_height' => 		'30', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'galleria-metropolia' . '_h4_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Prata', 
				'font_size' => 			'18', 
				'line_height' => 		'28', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'galleria-metropolia' . '_h5_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Karla:400,400i,700,700i', 
				'font_size' => 			'14', 
				'line_height' => 		'18', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'uppercase', 
				'text_decoration' => 	'none' 
			), 
			'galleria-metropolia' . '_h6_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Karla:400,400i,700,700i', 
				'font_size' => 			'12', 
				'line_height' => 		'16', 
				'font_weight' => 		'bold', 
				'font_style' => 		'normal', 
				'text_transform' => 	'uppercase', 
				'text_decoration' => 	'none' 
			) 
		), 
		'other' => array( 
			'galleria-metropolia' . '_button_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Karla:400,400i,700,700i', 
				'font_size' => 			'12', 
				'line_height' => 		'46', 
				'font_weight' => 		'bold', 
				'font_style' => 		'normal', 
				'text_transform' => 	'uppercase' 
			), 
			'galleria-metropolia' . '_small_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Muli', 
				'font_size' => 			'14', 
				'line_height' => 		'24', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none' 
			), 
			'galleria-metropolia' . '_input_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Muli', 
				'font_size' => 			'14', 
				'line_height' => 		'26', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal' 
			), 
			'galleria-metropolia' . '_quote_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Prata', 
				'font_size' => 			'22', 
				'line_height' => 		'38', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal' 
			) 
		), 
		'google' => array( 
			'galleria-metropolia' . '_google_web_fonts' => array( 
				'Karla:400,400i,700,700i|Karla', 
				'Muli|Muli',
				'Prata|Prata', 
				'Roboto:300,300italic,400,400italic,500,500italic,700,700italic|Roboto', 
				'Roboto+Condensed:400,400italic,700,700italic|Roboto Condensed', 
				'Open+Sans:300,300italic,400,400italic,700,700italic|Open Sans', 
				'Open+Sans+Condensed:300,300italic,700|Open Sans Condensed', 
				'Droid+Sans:400,700|Droid Sans', 
				'Droid+Serif:400,400italic,700,700italic|Droid Serif', 
				'PT+Sans:400,400italic,700,700italic|PT Sans', 
				'PT+Sans+Caption:400,700|PT Sans Caption', 
				'PT+Sans+Narrow:400,700|PT Sans Narrow', 
				'PT+Serif:400,400italic,700,700italic|PT Serif', 
				'Ubuntu:400,400italic,700,700italic|Ubuntu', 
				'Ubuntu+Condensed|Ubuntu Condensed', 
				'Headland+One|Headland One', 
				'Source+Sans+Pro:300,300italic,400,400italic,700,700italic|Source Sans Pro', 
				'Lato:400,400italic,700,700italic|Lato', 
				'Cuprum:400,400italic,700,700italic|Cuprum', 
				'Oswald:300,400,700|Oswald', 
				'Yanone+Kaffeesatz:300,400,700|Yanone Kaffeesatz', 
				'Lobster|Lobster', 
				'Lobster+Two:400,400italic,700,700italic|Lobster Two', 
				'Questrial|Questrial', 
				'Raleway:300,400,500,600,700|Raleway', 
				'Dosis:300,400,500,700|Dosis', 
				'Cutive+Mono|Cutive Mono', 
				'Quicksand:300,400,700|Quicksand', 
				'Montserrat:400,700|Montserrat', 
				'Cookie|Cookie', 
			) 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// WP Color Picker Palettes
if (!function_exists('cmsmasters_color_picker_palettes')) {

function cmsmasters_color_picker_palettes() {
	$palettes = array( 
		'#000000', 
		'#ffffff', 
		'#797979', 
		'#4c4c4c', 
		'#787b80', 
		'#16202f', 
		'#fafafa', 
		'#d8dadc' 
	);
	
	
	return $palettes;
}

}



// Theme Settings Color Schemes Default Colors
if (!function_exists('galleria_metropolia_color_schemes_defaults')) {

function galleria_metropolia_color_schemes_defaults($id = false) {
	$settings = array( 
		'default' => array( // content default color scheme
			'color' => 		'#797979', 
			'link' => 		'#4c4c4c', 
			'hover' => 		'#787b80', 
			'heading' => 	'#16202f', 
			'bg' => 		'#fafafa', 
			'alternate' => 	'#ffffff', 
			'border' => 	'#d8dadc' 
		), 
		'header' => array( // Header color scheme
			'mid_color' => 		'rgba(255,255,255,0.65)', 
			'mid_link' => 		'rgba(255,255,255,0.75)', 
			'mid_hover' => 		'#ffffff', 
			'mid_bg' => 		'#252930', 
			'mid_bg_scroll' => 	'#252930', 
			'mid_border' => 	'rgba(255,255,255,0.17)', 
			'bot_color' => 		'rgba(255,255,255,0.65)', 
			'bot_link' => 		'rgba(255,255,255,0.75)', 
			'bot_hover' => 		'#ffffff', 
			'bot_bg' => 		'#252930', 
			'bot_bg_scroll' => 	'#252930', 
			'bot_border' => 	'rgba(255,255,255,0.17)', 
			'overlaps_bg' =>	'rgba(255,255,255,0)'
		), 
		'navigation' => array( // Navigation color scheme
			'title_link' => 			'rgba(255,255,255,0.75)', 
			'title_link_hover' => 		'#ffffff', 
			'title_link_current' => 	'#ffffff', 
			'title_link_subtitle' => 	'rgba(255,255,255,0.5)', 
			'title_link_bg' => 			'rgba(255,255,255,0)', 
			'title_link_bg_hover' => 	'rgba(255,255,255,0)', 
			'title_link_bg_current' => 	'rgba(255,255,255,0)', 
			'title_link_border' => 		'rgba(255,255,255,0)', 
			'dropdown_text' => 			'rgba(255,255,255,0.5)', 
			'dropdown_bg' => 			'rgba(49,54,63,0.9)', 
			'dropdown_border' => 		'rgba(255,255,255,0)', 
			'dropdown_link' => 			'rgba(255,255,255,0.6)', 
			'dropdown_link_hover' => 	'#ffffff', 
			'dropdown_link_subtitle' => 'rgba(255,255,255,0.5)', 
			'dropdown_link_highlight' => 'rgba(255,255,255,0)', 
			'dropdown_link_border' => 	'rgba(255,255,255,0)' 
		), 
		'header_top' => array( // Header Top color scheme
			'color' => 					'rgba(255,255,255,0.65)', 
			'link' => 					'rgba(255,255,255,0.80)', 
			'hover' => 					'#ffffff', 
			'bg' => 					'#252930', 
			'border' => 				'rgba(255,255,255,0.17)', 
			'title_link' => 			'rgba(255,255,255,0.8)', 
			'title_link_hover' => 		'#ffffff', 
			'title_link_bg' => 			'rgba(255,255,255,0)', 
			'title_link_bg_hover' => 	'rgba(255,255,255,0)', 
			'title_link_border' => 		'rgba(255,255,255,0)', 
			'dropdown_bg' => 			'rgba(51,55,63,0.9)',  
			'dropdown_border' => 		'rgba(255,255,255,0)', 
			'dropdown_link' => 			'rgba(255,255,255,0.6)', 
			'dropdown_link_hover' => 	'#ffffff', 
			'dropdown_link_highlight' => 'rgba(255,255,255,0)', 
			'dropdown_link_border' => 	'rgba(255,255,255,0)' 
		), 
		'footer' => array( // Footer color scheme
			'color' => 		'#797979', 
			'link' => 		'#4c4c4c', 
			'hover' => 		'#787b80', 
			'heading' => 	'#3f454f', 
			'bg' => 		'#fafafa', 
			'alternate' => 	'#ffffff', 
			'border' => 	'#e3e4e5' 
		), 
		'first' => array( // custom color scheme 1
			'color' => 		'#797979', 
			'link' => 		'#4c4c4c', 
			'hover' => 		'#787b80', 
			'heading' => 	'#16202f', 
			'bg' => 		'#fafafa', 
			'alternate' => 	'#ffffff', 
			'border' => 	'#d8dadc' 
		), 
		'second' => array( // custom color scheme 2
			'color' => 		'#797979', 
			'link' => 		'#4c4c4c', 
			'hover' => 		'#787b80', 
			'heading' => 	'#16202f', 
			'bg' => 		'#fafafa', 
			'alternate' => 	'#ffffff', 
			'border' => 	'#d8dadc' 
		), 
		'third' => array( // custom color scheme 3
			'color' => 		'#797979', 
			'link' => 		'#4c4c4c', 
			'hover' => 		'#787b80', 
			'heading' => 	'#16202f', 
			'bg' => 		'#fafafa', 
			'alternate' => 	'#ffffff', 
			'border' => 	'#d8dadc' 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// Theme Settings Elements Default Values
if (!function_exists('galleria_metropolia_settings_element_defaults')) {

function galleria_metropolia_settings_element_defaults($id = false) {
	$settings = array( 
		'sidebar' => array( 
			'galleria-metropolia' . '_sidebar' => 	'' 
		), 
		'icon' => array( 
			'galleria-metropolia' . '_social_icons' => array( 
				'cmsmasters-icon-facebook-1|#|' . esc_html__('Facebook', 'galleria-metropolia') . '|true|rgba(255,255,255,0.4)|#ffffff', 
				'cmsmasters-icon-gplus-1|#|' . esc_html__('Google+', 'galleria-metropolia') . '|true|rgba(255,255,255,0.4)|#ffffff', 
				'cmsmasters-icon-instagram|#|' . esc_html__('Instagram', 'galleria-metropolia') . '|true|rgba(255,255,255,0.4)|#ffffff', 
				'cmsmasters-icon-twitter|#|' . esc_html__('Twitter', 'galleria-metropolia') . '|true|rgba(255,255,255,0.4)|#ffffff', 
				'cmsmasters-icon-youtube-play|#|' . esc_html__('YouTube', 'galleria-metropolia') . '|true|rgba(255,255,255,0.4)|#ffffff' 
			) 
		), 
		'lightbox' => array( 
			'galleria-metropolia' . '_ilightbox_skin' => 				'dark', 
			'galleria-metropolia' . '_ilightbox_path' => 				'vertical', 
			'galleria-metropolia' . '_ilightbox_infinite' => 			0, 
			'galleria-metropolia' . '_ilightbox_aspect_ratio' => 		1, 
			'galleria-metropolia' . '_ilightbox_mobile_optimizer' => 	1, 
			'galleria-metropolia' . '_ilightbox_max_scale' => 			1, 
			'galleria-metropolia' . '_ilightbox_min_scale' => 			0.2, 
			'galleria-metropolia' . '_ilightbox_inner_toolbar' => 		0, 
			'galleria-metropolia' . '_ilightbox_smart_recognition' => 	0, 
			'galleria-metropolia' . '_ilightbox_fullscreen_one_slide' =>0, 
			'galleria-metropolia' . '_ilightbox_fullscreen_viewport' => 'center', 
			'galleria-metropolia' . '_ilightbox_controls_toolbar' => 	1, 
			'galleria-metropolia' . '_ilightbox_controls_arrows' => 	0, 
			'galleria-metropolia' . '_ilightbox_controls_fullscreen' => 1, 
			'galleria-metropolia' . '_ilightbox_controls_thumbnail' => 	1, 
			'galleria-metropolia' . '_ilightbox_controls_keyboard' => 	1, 
			'galleria-metropolia' . '_ilightbox_controls_mousewheel' => 1, 
			'galleria-metropolia' . '_ilightbox_controls_swipe' => 		1, 
			'galleria-metropolia' . '_ilightbox_controls_slideshow' => 	0 
		), 
		'sitemap' => array( 
			'galleria-metropolia' . '_sitemap_nav' => 		1, 
			'galleria-metropolia' . '_sitemap_categs' => 	1, 
			'galleria-metropolia' . '_sitemap_tags' => 		1, 
			'galleria-metropolia' . '_sitemap_month' => 	1, 
			'galleria-metropolia' . '_sitemap_pj_categs' => 1, 
			'galleria-metropolia' . '_sitemap_pj_tags' => 	1 
		), 
		'error' => array( 
			'galleria-metropolia' . '_error_color' => 			'#292929', 
			'galleria-metropolia' . '_error_bg_color' => 		'#fbfbfb', 
			'galleria-metropolia' . '_error_bg_img_enable' => 	0, 
			'galleria-metropolia' . '_error_bg_image' => 		'', 
			'galleria-metropolia' . '_error_bg_rep' => 			'no-repeat', 
			'galleria-metropolia' . '_error_bg_pos' => 			'top center', 
			'galleria-metropolia' . '_error_bg_att' => 			'scroll', 
			'galleria-metropolia' . '_error_bg_size' => 		'cover', 
			'galleria-metropolia' . '_error_search' => 			1, 
			'galleria-metropolia' . '_error_sitemap_button' => 	1, 
			'galleria-metropolia' . '_error_sitemap_link' => 	'' 
		), 
		'code' => array( 
			'galleria-metropolia' . '_custom_css' => 		'', 
			'galleria-metropolia' . '_custom_js' => 		'', 
			'galleria-metropolia' . '_gmap_api_key' => 		'', 
			'galleria-metropolia' . '_api_key' => 			'', 
			'galleria-metropolia' . '_api_secret' => 		'', 
			'galleria-metropolia' . '_access_token' => 		'', 
			'galleria-metropolia' . '_access_token_secret' => '' 
		), 
		'recaptcha' => array( 
			'galleria-metropolia' . '_recaptcha_public_key' => 	'', 
			'galleria-metropolia' . '_recaptcha_private_key' => '' 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// Theme Settings Single Posts Default Values
if (!function_exists('galleria_metropolia_settings_single_defaults')) {

function galleria_metropolia_settings_single_defaults($id = false) {
	$settings = array( 
		'post' => array( 
			'galleria-metropolia' . '_blog_post_layout' => 			'r_sidebar', 
			'galleria-metropolia' . '_blog_post_title' => 			1, 
			'galleria-metropolia' . '_blog_post_date' => 			1, 
			'galleria-metropolia' . '_blog_post_date_settings' => 	1, 
			'galleria-metropolia' . '_blog_post_cat' => 			1, 
			'galleria-metropolia' . '_blog_post_author' => 			1, 
			'galleria-metropolia' . '_blog_post_comment' => 		1, 
			'galleria-metropolia' . '_blog_post_tag' => 			1, 
			'galleria-metropolia' . '_blog_post_like' => 			1, 
			'galleria-metropolia' . '_blog_post_nav_box' => 		1, 
			'galleria-metropolia' . '_blog_post_nav_order_cat' => 	0, 
			'galleria-metropolia' . '_blog_post_share_box' => 		1, 
			'galleria-metropolia' . '_blog_post_author_box' => 		1, 
			'galleria-metropolia' . '_blog_more_posts_box' => 		'popular', 
			'galleria-metropolia' . '_blog_more_posts_count' => 	'3', 
			'galleria-metropolia' . '_blog_more_posts_pause' => 	'5' 
		), 
		'project' => array( 
			'galleria-metropolia' . '_portfolio_project_title' => 			1, 
			'galleria-metropolia' . '_portfolio_project_details_title' => 	esc_html__('Project details', 'galleria-metropolia'), 
			'galleria-metropolia' . '_portfolio_project_date' => 			1, 
			'galleria-metropolia' . '_portfolio_project_cat' => 			1, 
			'galleria-metropolia' . '_portfolio_project_author' => 			1, 
			'galleria-metropolia' . '_portfolio_project_comment' => 		0, 
			'galleria-metropolia' . '_portfolio_project_tag' => 			0, 
			'galleria-metropolia' . '_portfolio_project_like' => 			1, 
			'galleria-metropolia' . '_portfolio_project_link' => 			0, 
			'galleria-metropolia' . '_portfolio_project_share_box' => 		1, 
			'galleria-metropolia' . '_portfolio_project_nav_box' => 		1, 
			'galleria-metropolia' . '_portfolio_project_nav_order_cat' => 	0, 
			'galleria-metropolia' . '_portfolio_project_author_box' => 		1, 
			'galleria-metropolia' . '_portfolio_more_projects_box' => 		'popular', 
			'galleria-metropolia' . '_portfolio_more_projects_count' => 	'4', 
			'galleria-metropolia' . '_portfolio_more_projects_pause' => 	'5', 
			'galleria-metropolia' . '_portfolio_project_slug' => 			'project', 
			'galleria-metropolia' . '_portfolio_pj_categs_slug' => 			'pj-categs', 
			'galleria-metropolia' . '_portfolio_pj_tags_slug' => 			'pj-tags' 
		), 
		'profile' => array( 
			'galleria-metropolia' . '_profile_post_title' => 			1, 
			'galleria-metropolia' . '_profile_post_details_title' => 	esc_html__('Profile details', 'galleria-metropolia'), 
			'galleria-metropolia' . '_profile_post_cat' => 				1, 
			'galleria-metropolia' . '_profile_post_comment' => 			1, 
			'galleria-metropolia' . '_profile_post_like' => 			1, 
			'galleria-metropolia' . '_profile_post_nav_box' => 			1, 
			'galleria-metropolia' . '_profile_post_nav_order_cat' => 	0, 
			'galleria-metropolia' . '_profile_post_share_box' => 		1, 
			'galleria-metropolia' . '_profile_post_slug' => 			'profile', 
			'galleria-metropolia' . '_profile_pl_categs_slug' => 		'pl-categs' 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



/* Project Puzzle Proportion */
if (!function_exists('galleria_metropolia_project_puzzle_proportion')) {

function galleria_metropolia_project_puzzle_proportion() {
	return 0.692;
}

}



/* Theme Image Thumbnails Size */
if (!function_exists('galleria_metropolia_get_image_thumbnail_list')) {

function galleria_metropolia_get_image_thumbnail_list() {
	$list = array( 
		'cmsmasters-small-thumb' => array( 
			'width' => 		180, 
			'height' => 	180, 
			'crop' => 		true 
		), 
		'cmsmasters-square-thumb' => array( 
			'width' => 		580, 
			'height' => 	580, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Square', 'galleria-metropolia') 
		), 
		'cmsmasters-blog-masonry-thumb' => array( 
			'width' => 		360, 
			'height' => 	460, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Masonry Blog', 'galleria-metropolia') 
		), 
		'cmsmasters-project-thumb' => array( 
			'width' => 		580, 
			'height' => 	420, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Project', 'galleria-metropolia') 
		), 
		'cmsmasters-project-masonry-thumb' => array( 
			'width' => 		580, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry Project', 'galleria-metropolia') 
		), 
		'post-thumbnail' => array( 
			'width' => 		860, 
			'height' => 	575, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Featured', 'galleria-metropolia') 
		), 
		'cmsmasters-masonry-thumb' => array( 
			'width' => 		860, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry', 'galleria-metropolia') 
		), 
		'cmsmasters-full-thumb' => array( 
			'width' => 		1160, 
			'height' => 	775, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Full', 'galleria-metropolia') 
		), 
		'cmsmasters-project-full-thumb' => array( 
			'width' => 		1160, 
			'height' => 	561, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Project Full', 'galleria-metropolia') 
		), 
		'cmsmasters-full-masonry-thumb' => array( 
			'width' => 		1160, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry Full', 'galleria-metropolia') 
		) 
	);
	
	
	return $list;
}

}



/* Project Post Type Registration Rename */
if (!function_exists('galleria_metropolia_project_labels')) {

function galleria_metropolia_project_labels() {
	return array( 
		'name' => 					esc_html__('Projects', 'galleria-metropolia'), 
		'singular_name' => 			esc_html__('Project', 'galleria-metropolia'), 
		'menu_name' => 				esc_html__('Projects', 'galleria-metropolia'), 
		'all_items' => 				esc_html__('All Projects', 'galleria-metropolia'), 
		'add_new' => 				esc_html__('Add New', 'galleria-metropolia'), 
		'add_new_item' => 			esc_html__('Add New Project', 'galleria-metropolia'), 
		'edit_item' => 				esc_html__('Edit Project', 'galleria-metropolia'), 
		'new_item' => 				esc_html__('New Project', 'galleria-metropolia'), 
		'view_item' => 				esc_html__('View Project', 'galleria-metropolia'), 
		'search_items' => 			esc_html__('Search Projects', 'galleria-metropolia'), 
		'not_found' => 				esc_html__('No projects found', 'galleria-metropolia'), 
		'not_found_in_trash' => 	esc_html__('No projects found in Trash', 'galleria-metropolia') 
	);
}

}

// add_filter('cmsmasters_project_labels_filter', 'galleria_metropolia_project_labels');


if (!function_exists('galleria_metropolia_pj_categs_labels')) {

function galleria_metropolia_pj_categs_labels() {
	return array( 
		'name' => 					esc_html__('Project Categories', 'galleria-metropolia'), 
		'singular_name' => 			esc_html__('Project Category', 'galleria-metropolia') 
	);
}

}

// add_filter('cmsmasters_pj_categs_labels_filter', 'galleria_metropolia_pj_categs_labels');


if (!function_exists('galleria_metropolia_pj_tags_labels')) {

function galleria_metropolia_pj_tags_labels() {
	return array( 
		'name' => 					esc_html__('Project Tags', 'galleria-metropolia'), 
		'singular_name' => 			esc_html__('Project Tag', 'galleria-metropolia') 
	);
}

}

// add_filter('cmsmasters_pj_tags_labels_filter', 'galleria_metropolia_pj_tags_labels');



/* Profile Post Type Registration Rename */
if (!function_exists('galleria_metropolia_profile_labels')) {

function galleria_metropolia_profile_labels() {
	return array( 
		'name' => 					esc_html__('Profiles', 'galleria-metropolia'), 
		'singular_name' => 			esc_html__('Profiles', 'galleria-metropolia'), 
		'menu_name' => 				esc_html__('Profiles', 'galleria-metropolia'), 
		'all_items' => 				esc_html__('All Profiles', 'galleria-metropolia'), 
		'add_new' => 				esc_html__('Add New', 'galleria-metropolia'), 
		'add_new_item' => 			esc_html__('Add New Profile', 'galleria-metropolia'), 
		'edit_item' => 				esc_html__('Edit Profile', 'galleria-metropolia'), 
		'new_item' => 				esc_html__('New Profile', 'galleria-metropolia'), 
		'view_item' => 				esc_html__('View Profile', 'galleria-metropolia'), 
		'search_items' => 			esc_html__('Search Profiles', 'galleria-metropolia'), 
		'not_found' => 				esc_html__('No Profiles found', 'galleria-metropolia'), 
		'not_found_in_trash' => 	esc_html__('No Profiles found in Trash', 'galleria-metropolia') 
	);
}

}

// add_filter('cmsmasters_profile_labels_filter', 'galleria_metropolia_profile_labels');


if (!function_exists('galleria_metropolia_pl_categs_labels')) {

function galleria_metropolia_pl_categs_labels() {
	return array( 
		'name' => 					esc_html__('Profile Categories', 'galleria-metropolia'), 
		'singular_name' => 			esc_html__('Profile Category', 'galleria-metropolia') 
	);
}

}

// add_filter('cmsmasters_pl_categs_labels_filter', 'galleria_metropolia_pl_categs_labels');

